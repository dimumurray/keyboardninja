package typedefs;

import openfl.media.Sound;
import openfl.media.SoundChannel;
import openfl.media.SoundTransform;

typedef AudioData = {
    sound:Sound,
    channel:SoundChannel,
    transform:SoundTransform
}
