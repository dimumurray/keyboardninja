package ash.components;

import openfl.Vector;
import openfl.geom.Matrix3D;
import openfl.geom.Vector3D;

class TransformVector {
    public var points:Vector<Float>;
    public var matrix:Matrix3D;
    public var pivot:Vector3D;

    public function new() {}
}
