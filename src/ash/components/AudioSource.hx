/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Damion Murray
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
package ash.components;

import openfl.media.Sound;
import openfl.media.SoundChannel;
import openfl.media.SoundTransform;

import openfl.Assets;

class AudioSource {
    public var id:String;
    public var type:String;

    @:isVar public var volume(get, set):Float;
    function get_volume():Float {
        return volume;
    }
    function set_volume(v:Float):Float {
        if (transform != null) {
            transform.volume = v;
            channel.soundTransform = transform;
        }

        return volume = v;
    }

    public var pan:Float = 0.0;

    public var startTime:Float = 0.0;
    public var loops:Int = 0;

    public var invalidate:Bool = false;
    public var stopOnNodeRemoval:Bool = false;

    public var sound:Sound = null;
    public var channel:SoundChannel = null;
    public var transform:SoundTransform = null;

    public var cacheSound:Bool = false;
    public var clearSoundCache:Bool = false;

    public function new() {}
}
