package ash.components;
class AudioSettings {
    public var soundEnabled:Bool = true;
    public var musicEnabled:Bool = true;

    public var musicToggled:Bool = false;
    public var soundToggled:Bool = false;

    public var invalidate:Bool = false;

    public function new() {}
}
