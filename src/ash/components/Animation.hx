package ash.components;

import openfl.geom.Rectangle;
import openfl.display.DisplayObject;

class Animation {
    public var numFrames(get, never):Int;
    function get_numFrames():Int {
        return (frames != null)? frames.length:0;
    }

    public var frames:Array<DisplayObject>;
    public var bounds:Rectangle;
    public var currentFrame:Int;

    public var period:Float = 0;

    public function new() {}
}
