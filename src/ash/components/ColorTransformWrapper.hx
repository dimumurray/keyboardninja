/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Damion Murray
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/

package ash.components;

import openfl.geom.ColorTransform;

class ColorTransformWrapper {
    // RED
    @:isVar public var red(get, set):Float;
    function get_red():Float {
        return red;
    }
    function set_red(v:Float):Float {
        colorTransform.redMultiplier = v;

        return red = v;
    }


    // GREEN
    @:isVar public var green(get, set):Float;
    function get_green():Float {
        return green;
    }
    function set_green(v:Float):Float {
        colorTransform.greenMultiplier = v;

        return green = v;
    }


    // BLUE
    @:isVar public var blue(get, set):Float;
    function get_blue():Float {
        return blue;
    }
    function set_blue(v:Float):Float {
        colorTransform.blueMultiplier = v;

        return blue = v;
    }


    // ALPHA
    @:isVar public var alpha(get, set):Float;
    function get_alpha():Float {
        return alpha;
    }

    function set_alpha(v:Float):Float {
        colorTransform.alphaMultiplier = v;

        return alpha = v;
    }


    // RGB
    @:isVar public var rgb(get, set):Float;
    function get_rgb():Float {
        return rgb;
    }

    function set_rgb(v:Float):Float {
        colorTransform.redMultiplier = v;
        colorTransform.greenMultiplier = v;
        colorTransform.blueMultiplier = v;

        return rgb = v;
    }


    // Red offset
    @:isVar public var redOffset(get, set):Float;
    function get_redOffset():Float {
        return redOffset;
    }

    function set_redOffset(v:Float):Float {
        colorTransform.redOffset = v;

        return redOffset = v;
    }

    // Green offset
    @:isVar public var greenOffset(get, set):Float;
    function get_greenOffset():Float {
        return greenOffset;
    }

    function set_greenOffset(v:Float):Float {
        colorTransform.greenOffset = v;

        return greenOffset = v;
    }

    // Blue offset
    @:isVar public var blueOffset(get, set):Float;
    function get_blueOffset():Float {
        return blueOffset;
    }

    function set_blueOffset(v:Float):Float {
        colorTransform.blueOffset = v;

        return blueOffset = v;
    }

    // Alpha offset
    @:isVar public var alphaOffset(get, set):Float;
    function get_alphaOffset():Float {
        return alphaOffset;
    }

    function set_alphaOffset(v:Float):Float {
        colorTransform.alphaOffset = v;

        return alphaOffset = v;
    }

    public var colorTransform:ColorTransform;
    public var invalidate:Bool = false;

    public function new() {}
}
