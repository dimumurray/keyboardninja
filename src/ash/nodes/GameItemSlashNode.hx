package ash.nodes;

import ash.core.Node;

import ash.components.Viewport;
import ash.components.Animation;
import ash.components.Transform2D;

import funtotype.components.GameItemInfo;
import funtotype.components.GameItemSlash;

class GameItemSlashNode extends Node<GameItemSlashNode> {
    public var info:GameItemInfo;
    public var slash:GameItemSlash;
    public var anim:Animation;
    public var transform:Transform2D;
    public var viewport:Viewport;

    public function new() {}
}
