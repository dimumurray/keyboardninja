package ash.nodes;

import ash.core.Node;
import ash.components.Viewport;
import ash.components.Transform2D;
import ash.components.TransformVector;
import ash.components.ViewportTransform;

import openfl.Vector;
import openfl.geom.Matrix3D;
import openfl.geom.Vector3D;
import openfl.geom.Rectangle;

class ViewportTransformNode extends Node<ViewportTransformNode> {
    public var viewport:Viewport;
    public var transform:Transform2D;
    public var tVector:TransformVector;
    public var vTransform:ViewportTransform;

    public function new() {}

    public function applyTransform():Void {


        var matrix:Matrix3D = tVector.matrix;
        var transformed:Vector<Float> = new Vector<Float>();
        var rect:Rectangle = viewport.rect;

        matrix.identity();

        if (transform.rotation != 0) {
            matrix.appendTranslation(-tVector.pivot.x, -tVector.pivot.y, 0);
            matrix.appendRotation(transform.rotation, Vector3D.Z_AXIS);
            matrix.appendTranslation(tVector.pivot.x, tVector.pivot.y, 0);
        }

        matrix.appendTranslation(transform.x, transform.y, 0);


        matrix.transformVectors(tVector.points, transformed);

        var x:Float;
        var y:Float;

        var minX:Float = 999999;
        var minY:Float = 999999;

        var maxX:Float = -999999;
        var maxY:Float = -999999;

        for (i in 0...4) {
            x = transformed[i * 3];
            y = transformed[(i * 3) + 1];

            minX = Math.min(minX, x);
            minY = Math.min(minY, y);

            maxX = Math.max(maxX, x);
            maxY = Math.max(maxY, y);
        }

        rect.x = minX;
        rect.y = minY;
        rect.width = maxX - minX;
        rect.height = maxY - minY;
    }

    public function reset():Void {
        var rect:Rectangle = viewport.rect;
        var points:Vector<Float> = tVector.points;

        rect.x = points[0];
        rect.y = points[1];
        rect.width = points[3] - points[0];
        rect.height = points[7] - points[1];

        transform.x = 0;
        transform.y = 0;
        transform.rotation = 0;
    }
}
