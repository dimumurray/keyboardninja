/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Damion Murray
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
package ash.factories;

import ash.components.*;
import ash.components.tags.*;
import funtotype.components.*;
import funtotype.enums.GameItemCategory;

import ash.fsm.EntityState;
import ash.fsm.EntityStateMachine;

import typedefs.TweenData;
import typedefs.ViewState;
import typedefs.BitmapLayer;

import openfl.geom.Point;
import openfl.geom.Matrix3D;
import openfl.geom.Vector3D;
import openfl.geom.Rectangle;
import openfl.geom.ColorTransform;

import openfl.display.BitmapData;
import openfl.display.DisplayObject;

import openfl.text.TextField;

import openfl.Vector;

import openfl.Assets;

class ComponentFactory {
    public function new() {}

    public function createTransform(
        x:Float = 0,
        y:Float = 0,
        rotation:Float = 0,
        scale:Float = 1
    ):Transform2D {
        var transform:Transform2D = new Transform2D();

        transform.x = x;
        transform.y = y;
        transform.rotation = rotation;
        transform.scale = scale;

        return transform;
    }

    public function createGameConfig(running:Bool = true):GameConfig {
        var state:GameConfig = new GameConfig();

        state.running = running;

        return state;
    }

    public function createHealth(lives:Int = 3, invalidate:Bool = false):Health {
        var health:Health = new Health();

        health.lives = lives;
        health.invalidate = invalidate;

        return health;
    }

    public function createGameStats(total:Int = 0, multiplier:Int = 1, invalidate:Bool = false):GameStats {
        var stats:GameStats = new GameStats();

        stats.pointTotal = total;
        stats.multiplier = multiplier;
        stats.invalidate = invalidate;

        return stats;
    }

    public function createViewportFromRect(rect:Rectangle):Viewport {
        var viewport:Viewport = new Viewport();

        viewport.rect = rect;

        return viewport;
    }

    public function createTransition(inTweens:Array<TweenData>, outTweens:Array<TweenData>):Transition {
        // create button states
        var transition:Transition = new Transition();

        transition.inTweens = inTweens;
        transition.outTweens = outTweens;

        return transition;
    }

    public function createFSMInfo(fsm:EntityStateMachine, update:Bool = false, curr:String = "", next:String = "", prev:String=""):FSMInfo {
        var info:FSMInfo = new FSMInfo();

        info.fsm = fsm;
        info.currentState = curr;
        info.prevState = prev;
        info.nextState = next;
        info.invalidate = update;

        return info;
    }

    public function createViewState(fsm:EntityStateMachine, contextData:ViewState):EntityState {
        var entityState:EntityState = fsm.createState(contextData.name);
        var ComponentClass:Class<Dynamic>;
        var componentInstance:Dynamic;

        for (componentData in contextData.components) {

            ComponentClass = Type.resolveClass(componentData.type);
            componentInstance = Type.createEmptyInstance(ComponentClass);

            for (prop in Reflect.fields(componentData.params)) {
                Reflect.setProperty(componentInstance, prop, Reflect.field(componentData.params, prop));
            }

            entityState.add(ComponentClass).withInstance(componentInstance);

        }

        return entityState;
    }

    public function createCharacter(textField:TextField, keyCode:Int = -1):Character {
        var char:Character = new Character();

        char.textField = textField;
        char.keyCode = keyCode;

        return char;
    }

    public function createColorTransformWrapper(invalidate:Bool = false):ColorTransformWrapper {
        var cTransform:ColorTransformWrapper = new ColorTransformWrapper();

        cTransform.colorTransform = new ColorTransform();
        cTransform.invalidate = invalidate;

        return cTransform;
    }

    public function createBitmapSource(bitmap:BitmapData):BitmapSource {
        var source:BitmapSource = new BitmapSource();

        source.bitmap = bitmap;

        return source;
    }

    public function createOffset(p:Point):Offset {
        var offset:Offset = new Offset();

        offset.point = p;

        return offset;
    }

    public function createColorBufferMap(map:Map<Int, String>):ColorBufferMap {
        var buffer:ColorBufferMap = new ColorBufferMap();

        buffer.map = map;

        return buffer;
    }

    public function createNewton(mass:Float, maxSpeed:Float, vx:Float = 0, vy:Float = 0, torque:Float = 0, ax:Float = 0, ay:Float = 0):Newton {
        var newton:Newton = new Newton();

        newton.mass = mass;
        newton.maxSpeed = maxSpeed;
        newton.vx = vx;
        newton.vy = vy;
        newton.torque = torque;
        newton.ax = ax;
        newton.ay = ay;

        return newton;
    }

    public function createInGame():InGame {
        return new InGame();
    }

    public function createGameItemInfo(type:String, category:GameItemCategory, sliceTrajectory:Point, width:Float, height:Float, splatterColor:String, layers:Array<BitmapLayer>): GameItemInfo {
        var info:GameItemInfo = new GameItemInfo();

        info.type = type;
        info.category = category;
        info.sliceTrajectory = sliceTrajectory;
        info.width = width;
        info.height = height;
        info.splatterColor = splatterColor;
        info.layers = layers;

        return info;
    }

    public function createAudioSource(id:String, type:String, loops:Int = 0, vol:Float = 1.0, pan:Float = 0.0, startTime:Float=0.0):AudioSource {
        var audio:AudioSource = new AudioSource();

        audio.id = id;
        audio.type = type;
        audio.volume = vol;
        audio.pan = pan;
        audio.startTime = startTime;
        audio.loops = loops;

        return audio;
    }

    public function createIceVignetteTag():IceVignetteTag {
        var tag:IceVignetteTag = new IceVignetteTag();
        return tag;
    }

    public function createAlphaTransform(alpha:Int):AlphaTransform {
        var aTransform:AlphaTransform = new AlphaTransform();

        aTransform.alpha = alpha;

        return aTransform;
    }

    public function createTransient(duration:Float = 0.0):Transient {
        var transient:Transient = new Transient();

        transient.duration = duration;

        return transient;
    }

    public function createStagger(length:Float):Stagger {
        var stagger:Stagger = new Stagger();

        stagger.length = length;

        return stagger;
    }

    public function createSplatter(color:Int = 0, layer:BitmapLayer):Splatter {
        var splatter:Splatter = new Splatter();

        splatter.layer = layer;
        splatter.color = color;

        return splatter;
    }

    public function createTweenSequence(tweens:Array<TweenData>):TweenSequence {
        var sequence:TweenSequence = new TweenSequence();

        sequence.tweens = tweens;

        return sequence;
    }

    public function createPopUp(layer:BitmapLayer = null):PopUp {
        var popUp:PopUp = new PopUp();

        popUp.layer = layer;

        return popUp;
    }

    public function createMenuButtonInfo(name:String, layers:Array<BitmapLayer>):MenuButtonInfo {
        var info:MenuButtonInfo = new MenuButtonInfo();

        info.name = name;
        info.layers = layers;

        return info;
    }

    public function createAnimation(frames:Array<DisplayObject>):Animation {
        var animation:Animation = new Animation();

        animation.frames = frames;
        animation.bounds = new Rectangle(0, 0, frames[0].width, frames[0].height);
        animation.currentFrame = 0;

        return animation;
    }

    public function createAudioSettings(soundEnabled:Bool = true, musicEnabled:Bool = true):AudioSettings {
        var settings:AudioSettings = new AudioSettings();

        settings.soundEnabled = soundEnabled;
        settings.musicEnabled = musicEnabled;

        return settings;
    }

    public function createTransformVector(points:Vector<Float>, pivot:Vector3D):TransformVector {
        var tVector:TransformVector = new TransformVector();

        tVector.points = points;
        tVector.matrix = new Matrix3D();
        tVector.pivot = pivot;

        return tVector;
    }

    public function createViewportTransform(resetDelay:Float = 0.0):ViewportTransform {
        var tView:ViewportTransform = new ViewportTransform();

        tView.resetDelay = resetDelay;

        return tView;
    }

    public function createMenuSlash():MenuSlash {
        var slash:MenuSlash = new MenuSlash();
        return slash;
    }

    public function createGameItemSlash():GameItemSlash {
        var slash:GameItemSlash = new GameItemSlash();
        return slash;
    }

    public function createGameItemExplosion():GameItemExplosion {
        var explosion:GameItemExplosion = new GameItemExplosion();
        return explosion;
    }

    public function createBloomEffect(base:BitmapData):BloomEffect {
        var effect:BloomEffect = new BloomEffect();

        effect.base = base;
        return effect;
    }
}