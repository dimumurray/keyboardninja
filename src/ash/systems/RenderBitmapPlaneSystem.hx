/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Damion Murray
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/

package ash.systems;

import openfl.display.Bitmap;
import openfl.filters.ColorMatrixFilter;
import ash.core.System;
import ash.core.NodeList;
import ash.core.Engine;
import ash.nodes.RenderBitmapPlaneNode;

import openfl.display.BitmapData;
import openfl.geom.ColorTransform;

class RenderBitmapPlaneSystem extends System {
    private var nodes:NodeList<RenderBitmapPlaneNode>;
    private var canvas:BitmapData;

    public function new(_canvas:BitmapData) {
        super();
        this.canvas = _canvas;
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        nodes = engine.getNodeList(RenderBitmapPlaneNode);

    }

    override public function update(time:Float):Void {
        var update:Bool = false;
        var colorTransformUpdated:Bool = false;

        for (node in nodes) {

            if (node.fader.invalidate || (node.source.invalidate && node.source.hasContentToRender)) {
                update = true;
                break;
            }

        }

        if (update) {

            canvas.lock();

            var colorTransform:ColorTransform = new ColorTransform();

            for (node in nodes) {

                if (node.source.hasContentToRender) {

                    canvas.copyPixels(
                        node.source.bitmap,
                        node.viewport.rect,
                        node.offset.point,
                        null,
                        null,
                        true
                    );

                    node.source.invalidate = false;

                }

                if (node.fader.invalidate) {
                    colorTransform.redMultiplier *= node.fader.colorTransform.redMultiplier;
                    colorTransform.greenMultiplier *= node.fader.colorTransform.greenMultiplier;
                    colorTransform.blueMultiplier *= node.fader.colorTransform.blueMultiplier;
                    colorTransform.alphaMultiplier *= node.fader.colorTransform.alphaMultiplier;

                    node.fader.invalidate = false;
                    colorTransformUpdated = true;
                }

            }

            if (colorTransformUpdated) {
                canvas.colorTransform(canvas.rect, colorTransform);
            }

            canvas.unlock();

        }
    }
}
