/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Damion Murray
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
package ash.systems;

import ash.core.Engine;
import ash.core.System;
import ash.core.NodeList;
import ash.nodes.AudioNode;

import ash.components.Transient;
import ash.components.AudioSource;
import ash.components.AudioSettings;

import openfl.Assets;
import openfl.media.Sound;
import openfl.media.SoundChannel;
import openfl.media.SoundTransform;

import typedefs.AudioData;

class AudioSystem extends System {
    private var nodes:NodeList<AudioNode>;

    private var soundCache:Map<String, AudioData>;
    private var audioSettings:AudioSettings;

    public function new() {
        super();
        soundCache = new Map<String, AudioData>();
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        nodes = engine.getNodeList(AudioNode);
        audioSettings = engine.getEntityByName("gameSession").get(AudioSettings);
        for (node in nodes) {
            onNodeAdded(node);
        }

        nodes.nodeAdded.add(onNodeAdded);
        nodes.nodeRemoved.add(onNodeRemoved);
    }

    private function onNodeAdded(node:AudioNode):Void {

        if (soundCache.exists(node.source.id)) {
            var data:AudioData = soundCache.get(node.source.id);

            node.source.sound = data.sound;
            node.source.channel = data.channel;
            node.source.transform = data.transform;

        } else {

            switch(node.source.type) {
                case "music":
                    node.source.sound = Assets.getMusic(node.source.id);
                case "sound":
                    node.source.sound = Assets.getSound(node.source.id);
            }

            var enabled:Bool = (node.source.type == "music")?audioSettings.musicEnabled:audioSettings.soundEnabled;

            node.source.transform = new SoundTransform((enabled)?node.source.volume:0, node.source.pan);
            node.source.channel = node.source.sound.play(node.source.startTime, node.source.loops, node.source.transform);

            if (node.source.cacheSound) {

                soundCache.set(
                    node.source.id,
                    {
                        sound:node.source.sound,
                        channel:node.source.channel,
                        transform:node.source.transform
                    }
                );

            }

        }

        if(node.entity.has(Transient)) {
            var transient:Transient = node.entity.get(Transient);
            transient.duration = (Math.ceil(node.source.sound.length/1000) * (1 + node.source.loops)) + 0.5;
        }

    }

    private function onNodeRemoved(node:AudioNode):Void {
        if (node.source.stopOnNodeRemoval) {
            node.source.channel.stop();
        }

        if (node.source.clearSoundCache) {
            if (soundCache.exists(node.source.id)) {
                soundCache.remove(node.source.id);
            }
        }
    }

    override public function update(time:Float):Void {

        if(audioSettings.invalidate) {

            var volume:Float;
            var transform:SoundTransform;

            for (node in nodes) {

                switch(node.source.type) {

                    case "music":
                        if (audioSettings.musicToggled) {
                            node.source.volume = (audioSettings.musicEnabled)?1.0:0.0;
                        }

                    case "sound":
                        if (audioSettings.soundToggled) {
                            node.source.volume = (audioSettings.soundEnabled)?1.0:0.0;
                        }

                }

            }

            audioSettings.musicToggled = false;
            audioSettings.soundToggled = false;
            audioSettings.invalidate = false;

        }

    }

}
