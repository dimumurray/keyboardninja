/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Damion Murray
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
package ash.systems;

import ash.interfaces.INetForceSource;
import ash.core.NodeList;
import ash.core.Engine;
import ash.tools.ListIteratingSystem;

import funtotype.components.GameConfig;
import ash.nodes.ForceResolverNode;

class ForceResolverSystem extends ListIteratingSystem<ForceResolverNode> {
    private var nodes:NodeList<ForceResolverNode>;
    private var state:GameConfig;
    private var netForceSource:INetForceSource;

    public function new(netForceSource:INetForceSource) {
        super(ForceResolverNode, updateNode);
        this.netForceSource = netForceSource;
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        nodes = engine.getNodeList(ForceResolverNode);
    }

    public function updateNode(node:ForceResolverNode, time:Float):Void {
        node.applyTorque(netForceSource.getTimeScale());
        node.applyForce(0, netForceSource.getNetForce());
        node.resolve(netForceSource.getTimeScale());
    }
}
