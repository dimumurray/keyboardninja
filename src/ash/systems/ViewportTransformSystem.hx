package ash.systems;

import ash.nodes.ViewportTransformNode;
import ash.core.NodeList;
import ash.core.Engine;
import ash.core.System;

import motion.Actuate;

class ViewportTransformSystem extends System {
    private var nodes:NodeList<ViewportTransformNode>;

    public function new() {
        super();
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        nodes = engine.getNodeList(ViewportTransformNode);

        nodes.nodeRemoved.add(onNodeRemoved);
    }

    private function onNodeRemoved(node:ViewportTransformNode):Void {
        Actuate.timer(node.vTransform.resetDelay).onComplete(node.reset);
    }

    override public function update(time:Float):Void {
        super.update(time);

        for (_node in nodes) {
            var node:ViewportTransformNode = cast(_node, ViewportTransformNode);

            if (node.transform.invalidate) {
                node.applyTransform();
                node.transform.invalidate = false;
            }
        }
    }

}
