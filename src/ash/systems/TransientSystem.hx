/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Damion Murray
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
package ash.systems;

import ash.core.Engine;
import ash.nodes.TransientNode;
import ash.tools.ListIteratingSystem;

class TransientSystem extends ListIteratingSystem<TransientNode> {
    private var engine:Engine;

    public function new() {
        super(TransientNode, updateNode);
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);
        this.engine = engine;
    }

    public function updateNode(node:TransientNode, time:Float):Void {
        if (node.transient.duration <= 0) {
            engine.removeEntity(node.entity);
        } else {
            node.transient.duration -= time;
        }
    }
}
