/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Damion Murray
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
package ash.systems;

import motion.actuators.GenericActuator;
import motion.Actuate;
import ash.core.Engine;
import ash.core.System;
import ash.core.NodeList;

import ash.components.*;
import ash.components.TweenSequence;

import funtotype.components.*;

import motion.easing.Cubic;
import motion.easing.Cubic.easeIn;
import motion.easing.Cubic.easeOut;
import motion.easing.Cubic.easeInOut;


import ash.nodes.ComponentTweenNode;

class ComponentTweenSystem extends System {
    private var nodes:NodeList<ComponentTweenNode>;

    public function new() {
        super();
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        nodes = engine.getNodeList(ComponentTweenNode);

        for (node in nodes) {
            onNodeAdded(node);
        }

        nodes.nodeAdded.add(onNodeAdded);
    }

    private function onNodeAdded(node:ComponentTweenNode):Void {
        var component:Dynamic;
        var componentClass:Class<Dynamic>;
        var sequence:TweenSequence = node.sequence;
        var actuator:GenericActuator<Dynamic> = null;

        for (tween in sequence.tweens) {
            componentClass = Type.resolveClass(tween.component);
            component = node.entity.get(componentClass);

            for (prop in Type.getInstanceFields(componentClass)) {

                if (Reflect.hasField(tween.from, prop)) {
                    Reflect.setProperty(component, prop, Reflect.field(tween.from, prop));
                }

            }

            actuator = Actuate
                .tween(component, tween.duration, tween.to, tween.overwrite)
                .delay(tween.delay)
                .ease(Reflect.getProperty(Type.resolveClass(tween.ease.type), tween.ease.func));
        }

        if (actuator != null)  {
            actuator.onComplete(removeTweenSequence, [node]);
        }

    }

    private function removeTweenSequence(node:ComponentTweenNode):Void {
        if (node.entity != null) node.entity.remove(TweenSequence);
    }
}
