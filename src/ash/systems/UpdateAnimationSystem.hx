package ash.systems;

import ash.core.Engine;
import ash.core.System;
import ash.core.NodeList;
import ash.nodes.UpdateAnimationNode;

class UpdateAnimationSystem extends System {
    private var nodes:NodeList<UpdateAnimationNode>;

    public function new() {
        super();
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        nodes = engine.getNodeList(UpdateAnimationNode);
    }

    override public function update(time:Float):Void {
        for (aNode in nodes) {
            var node:UpdateAnimationNode = cast(aNode, UpdateAnimationNode);
            node.anim.currentFrame = Math.floor(node.anim.period * (node.anim.numFrames - 1));
        }
    }

}
