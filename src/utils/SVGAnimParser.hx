/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Damion Murray
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
package utils;

import openfl.display.CapsStyle;
import openfl.display.JointStyle;
import openfl.display.Shape;
import openfl.display.Graphics;
import openfl.display.DisplayObject;

import openfl.errors.Error;

import Xml;
import Std;

class SVGAnimParser {
    static private var xyPattern:EReg = ~/([-+]?\d+\.?\d*) ?([-+]?\d+\.?\d*)/;
    static private var bezTuplePattern:EReg = ~/([-+]?\d+\.?\d*) ?([-+]?\d+\.?\d*) ?([-+]?\d+\.?\d*) ?([-+]?\d+\.?\d*) ?([-+]?\d+\.?\d*) ?([-+]?\d+\.?\d*)/;

    static private var strokePattern:EReg = ~/stroke[:][#]([a-fA-F0-9]{6})[;]/;
    static private var strokeWidthPattern:EReg = ~/stroke-width[:]([-+]?\d+\.?\d*)[;]/;
    static private var strokeLineCapPattern:EReg = ~/stroke-linecap[:](butt|round|square)[;]/;
    static private var strokeLineJoinPattern:EReg = ~/stroke-linejoin[:](miter|round|bevel)[;]/;
    static private var strokeMiterLimitPattern:EReg = ~/stroke-miterlimit[:]([-+]?\d+\.?\d*)[;]/;
    static private var strokeOpacityPattern:EReg = ~/stroke-opacity[:]([-+]?\d+\.?\d*)[;]/;

    static private var fillPattern:EReg = ~/fill[:][#]([a-fA-F0-9]{6})[;]/;
    static private var fillOpacityPattern:EReg = ~/fill-opacity[:]([-+]?\d+\.?\d*)[;]/;

    static private var capsMap:Dynamic = {butt:CapsStyle.NONE, round:CapsStyle.ROUND, square:CapsStyle.SQUARE};
    static private var joinMap:Dynamic = {miter:JointStyle.MITER, round:JointStyle.ROUND, bevel:JointStyle.BEVEL};

    static public function parse(xml:Xml, drawBoundingBox = true):Array<DisplayObject> {
        var root:Xml = xml.firstChild();
        var frames:Iterator<Xml> = root.elements();
        var paths:Iterator<Xml>;
        var data:String;
        var style:String;
        var shape:Shape;
        var g:Graphics;

        var fill:Int;
        var fillOpacity:Float;

        var animation:Array<DisplayObject> = new Array<DisplayObject>();

        var width:Float = Std.parseFloat(root.get("width"));
        var height:Float = Std.parseFloat(root.get("height"));

        for (frame in frames) {
            paths = frame.elements();
            shape = new Shape();
            g = shape.graphics;

            if (drawBoundingBox) {
                g.beginFill(0,0);
                g.drawRect(0,0, width, height);
                g.endFill();
            }

            for (path in paths) {
                data = path.get("d");
                style = path.get("style");

                if (fillPattern.match(style)) {
                    g.beginFill(
                        Std.parseInt("0x" + fillPattern.matched(1)),
                        (fillOpacityPattern.match(style))?Std.parseFloat(fillOpacityPattern.matched(1)):1
                    );
                }

                g.lineStyle(
                    strokeWidthPattern.match(style)?Std.parseFloat(strokeWidthPattern.matched(1)):null,
                    strokePattern.match(style)?Std.parseInt("0x" + strokePattern.matched(1)):null,
                    strokeOpacityPattern.match(style)?Std.parseFloat(strokePattern.matched(1)):null,
                    true,
                    null,
                    strokeLineCapPattern.match(style)?Reflect.field(capsMap, strokeLineCapPattern.matched(1)):null,
                    strokeLineJoinPattern.match(style)?Reflect.field(joinMap, strokeLineJoinPattern.matched(1)):null,
                    strokeMiterLimitPattern.match(style)?Std.parseFloat(strokeMiterLimitPattern.matched(1)):null
                );

                if (data != null) {

                    // get first match
                    if(xyPattern.match(data)) {

                        g.moveTo(
                            Std.parseFloat(xyPattern.matched(1)),
                            Std.parseFloat(xyPattern.matched(2))
                        );

                    } else {
                        throw new Error("SVGAnimParser::parse() -- Invalid data!!");
                    }

                    data = xyPattern.matchedRight();

                    while(bezTuplePattern.match(data)) {
                        g.cubicCurveTo(
                            Std.parseFloat(bezTuplePattern.matched(1)), Std.parseFloat(bezTuplePattern.matched(2)),
                            Std.parseFloat(bezTuplePattern.matched(3)), Std.parseFloat(bezTuplePattern.matched(4)),
                            Std.parseFloat(bezTuplePattern.matched(5)), Std.parseFloat(bezTuplePattern.matched(6))
                        );

                        data = bezTuplePattern.matchedRight();
                    }

                }

                if (fillPattern.match(style)) {
                    g.endFill();
                }

            }

            animation.push(shape);

        }

        return animation;

    }

}
