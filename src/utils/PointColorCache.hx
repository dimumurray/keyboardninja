/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Damion Murray
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
package utils;

import openfl.events.TouchEvent;
import openfl.events.Event;
import openfl.geom.Rectangle;
import openfl.display.Stage;
import openfl.display.BitmapData;
import openfl.events.MouseEvent;

import haxe.ds.ObjectMap;

import Std.int;

class PointColorCache {

    private var queue:Array<String>;
    private var enabled:Bool;
    private var currentKey:String;
    private var lastFetchedKey:String;

    private var buffer:BitmapData;
    private var viewport:Rectangle;
    private var colorMap:Map<Int, String>;

    public function new(stage:Stage, _buffer:BitmapData, _viewport:Rectangle, _colorMap:Map<Int, String>) {
        queue = new Array<String>();
        enabled = true;

        buffer = _buffer;
        viewport = _viewport;
        colorMap = _colorMap;

        currentKey = "";

        stage.addEventListener(TouchEvent.TOUCH_BEGIN, onClick);
        stage.addEventListener(MouseEvent.CLICK, onClick);
    }

    private function onClick(e:Dynamic):Void {
        if (!enabled) {
            return;
        }

        var color:UInt = buffer.getPixel32(
            int(viewport.x + e.stageX),
            int(viewport.y + e.stageY)
        );

        if (queue.length == 0) {
            currentKey = "";
        }

        if (((color & 0xFF000000) > 0)                      // color's alpha > 0
                && colorMap.exists(color)                 // color is a key in the color map
                && (currentKey != colorMap.get(color))      // color is new selection
        ){
            currentKey = colorMap.get(color);
            queue.push(currentKey);
        }
    }

    public function getTarget():String {
        lastFetchedKey = (queue.length > 0)?queue.shift():null;
        return lastFetchedKey;
    }

    public function getLastFetched():String {
        return lastFetchedKey;
    }

    public function disable():Void {
        enabled = false;
    }

    public function enable():Void {
        enabled = true;
    }

    public function clearCache():Void {
        queue = new Array<String>();
        currentKey = lastFetchedKey = "";
    }

    public function length():Int {
        return queue.length;
    }
}