package funtotype.factories;

import openfl.display.BitmapData;
import funtotype.components.Character;
import ash.core.Entity;
import motion.Actuate;
import ash.components.TransformVector;
import ash.components.Transform2D;
import ash.components.ViewportTransform;
import openfl.geom.Vector3D;
import motion.easing.Cubic;

import ash.core.Engine;
import ash.core.Entity;

import ash.fsm.EntityState;
import ash.fsm.EntityStateMachine;

import ash.components.Newton;
import ash.components.Offset;
import ash.components.FSMInfo;
import ash.components.Viewport;
import ash.components.Animation;
import ash.components.TweenSequence;
import ash.components.AlphaTransform;
import ash.components.ColorTransformWrapper;

import ash.factories.ComponentFactory;

import funtotype.enums.GameItemCategory;
import funtotype.components.GameItemInfo;
import funtotype.components.MenuButtonInfo;
import funtotype.nodes.GameItemNode;
import funtotype.nodes.MenuButtonNode;

import openfl.ui.Keyboard;
import openfl.errors.Error;

import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.GridFitType;
import openfl.text.AntiAliasType;
import openfl.text.TextFormatAlign;
import openfl.text.TextFieldAutoSize;

import openfl.geom.Point;
import openfl.geom.Matrix;
import openfl.geom.Rectangle;

import openfl.display.BitmapData;
import openfl.display.DisplayObject;

import openfl.Assets;
import openfl.Vector;

import typedefs.ViewContext;
import typedefs.BitmapLayer;

import Std;
import utils.SVGAnimParser;

class EntityFactory {
    private var engine:Engine;
    private var componentFactory:ComponentFactory;

    private var splatterLayers:Array<BitmapLayer>;

    private var fruitAlphaOutTween:TweenSequence;
    private var splatterAlphaOutTween:TweenSequence;
    private var popUpAlphaOutTween:TweenSequence;
    private var slashTween:TweenSequence;
    private var explodeTween:TweenSequence;

    private var slashFrames:Array<DisplayObject>;
    private var explodeFrames:Array<DisplayObject>;

    private var fillAlpha:BitmapData;

    public function new(_engine:Engine, factory:ComponentFactory, _splatterLayers:Array<BitmapLayer>) {
        engine = _engine;
        componentFactory = factory;
        splatterLayers = _splatterLayers;

        fruitAlphaOutTween = componentFactory.createTweenSequence(
            [
                {
                    "component":"ash.components.AlphaTransform",
                    "delay":0.1,
                    "duration":0.5,
                    "from":{},
                    "to":{"alpha":0},
                    "overwrite":true,
                    "ease":{
                        "type":"motion.easing.Cubic",
                        "func":"easeOut"
                    }
                }
            ]
        );

        splatterAlphaOutTween = componentFactory.createTweenSequence(
            [
                {
                    "component":"ash.components.AlphaTransform",
                    "delay":1,
                    "duration":1,
                    "from":{},
                    "to":{"alpha":0},
                    "overwrite":true,
                    "ease":{
                        "type":"motion.easing.Cubic",
                        "func":"easeOut"
                    }
                }
            ]
        );

        popUpAlphaOutTween = componentFactory.createTweenSequence(
            [
                {
                    "component":"ash.components.AlphaTransform",
                    "delay":0,
                    "duration":0.75,
                    "from":{},
                    "to":{"alpha":0xFF},
                    "overwrite":true,
                    "ease":{
                        "type":"motion.easing.Cubic",
                        "func":"easeIn"
                    }
                },
                {
                    "component":"ash.components.AlphaTransform",
                    "delay":1.75,
                    "duration":0.5,
                    "from":{},
                    "to":{"alpha":0},
                    "overwrite":false,
                    "ease":{
                        "type":"motion.easing.Cubic",
                        "func":"easeOut"
                    }
                }
            ]
        );

        slashTween = componentFactory.createTweenSequence(
            [
                {
                    "component":"ash.components.Animation",
                    "delay":0,
                    "duration":.5,
                    "from":{"period":0},
                    "to":{"period":1},
                    "overwrite":true,
                    "ease":{
                        "type":"motion.easing.Linear",
                        "func":"easeNone"
                    }
                }
            ]
        );

        explodeTween = componentFactory.createTweenSequence(
            [
                {
                    "component":"ash.components.Animation",
                    "delay":0,
                    "duration":1.5,
                    "from":{"period":0},
                    "to":{"period":1},
                    "overwrite":true,
                    "ease":{
                        "type":"motion.easing.Linear",
                        "func":"easeNone"
                    }
                },
                {
                    "component":"ash.components.ColorTransformWrapper",
                    "delay":0.5,
                    "duration":1,
                    "from":{"alphaOffset":0},
                    "to":{"alphaOffset":192},
                    "overwrite":true,
                    "ease":{
                    "type":"motion.easing.Linear",
                    "func":"easeNone"
                    }
                }
            ]
        );

        fillAlpha = Assets.getBitmapData("images/fill_alpha.png");
        slashFrames = SVGAnimParser.parse(Xml.parse(Assets.getText("slashAnim")));
        explodeFrames = SVGAnimParser.parse(Xml.parse(Assets.getText("explodeAnim")), false);
    }

    public function createGameSession(fsm:FSMInfo):Entity {
        var game:Entity = new Entity("gameSession");

        game
            .add(fsm)
            .add(componentFactory.createGameStats())
            .add(componentFactory.createHealth())
            .add(componentFactory.createGameConfig())
            .add(componentFactory.createAudioSettings());

        engine.addEntity(game);

        return game;
    }

    public function createRenderPlane(name:String, source:BitmapData, bounds:Rectangle, destPoint:Point):Entity {
        var plane:Entity = new Entity(name);

        plane
            .add(componentFactory.createColorTransformWrapper())
            .add(componentFactory.createBitmapSource(source))
            .add(componentFactory.createViewportFromRect(bounds))
            .add(componentFactory.createOffset(destPoint))
            .add(componentFactory.createTransform(bounds.x, bounds.y));

        engine.addEntity(plane);

        return plane;
    }

    /**
     *
     * @param name      - Entity name
     * @param info      - Finite State Machine info object; tracks the state of the FSM
     **/
    public function createButtonBackBuffer(name:String, info:FSMInfo):Entity {
        var buffer:Entity = new Entity(name);

        buffer
            .add(info);

        engine.addEntity(buffer);

        return buffer;
    }

    public function addViewFSM(e:Entity, context:ViewContext):Entity {
        var fsm:EntityStateMachine = new EntityStateMachine(e);
        var info:FSMInfo = componentFactory.createFSMInfo(fsm, false);
        var entityState:EntityState;

        e.add(info);

        // create states
        for (state in context.states) {
            entityState = componentFactory.createViewState(fsm, state);
        }

        fsm.changeState(context.initState);
        //info.nextState = context.initState;
        //info.invalidate = true;

        return e;
    }

    public function createIceVignette(name:String, source:BitmapData, bounds:Rectangle, dest:Point):Entity {
        var vignette:Entity = new Entity(name);

        // var cTransform:ColorTransformWrapper = new ColorTransformWrapper();
        // cTransform.alpha = 0;

        vignette.add(componentFactory.createIceVignetteTag())
                .add(componentFactory.createBitmapSource(source))
                .add(componentFactory.createViewportFromRect(bounds))
                .add(componentFactory.createOffset(dest))
                .add(componentFactory.createAlphaTransform(0xFF));

        engine.addEntity(vignette);

        return vignette;
    }

    public function createGameItem(info:GameItemInfo, speed:Float, letter:String = ""):Entity {
        var gameItem:Entity = new Entity();
        var rotation:Float = Math.random() * 360;
        var spread:Int = 150;

        // Start Position
        var x:Float = 250 + Math.floor(Math.random() * (666 - 250 + 1));
        var y:Float = 420;
        var torque:Float = 1 + Math.floor(Math.random() * 6);

        // Calcuate velocity vector
        var ex:Float = x - spread + Math.floor((spread + spread + 1) * Math.random());
        var ey:Float = 25 + Math.floor(Math.random() * (150 - 25 + 1));

        var vx:Float = ex - x;
        var vy:Float = ey - y;

        // Normalize and scale velocity vector
        var mag:Float = Math.sqrt((vx * vx) + (vy * vy));

        vx = (vx/mag) * speed;
        vy = (vy/mag) * (speed + speed);

        var format:TextFormat = new TextFormat("Garamond Bold", 72, (info.type == "bomb") ? 0xFF00FF : 0xFFFF00);
        format.align = TextFormatAlign.CENTER;

        var textField:TextField = new TextField();
        textField.embedFonts = true;
        textField.autoSize = TextFieldAutoSize.CENTER;
        textField.antiAliasType = AntiAliasType.ADVANCED;
        textField.gridFitType = GridFitType.PIXEL;
        textField.text = letter;
        textField.defaultTextFormat = format;
        textField.y = -textField.height;

        var keyCode:Int = -1;

        if (letter != "") {
            var charCode:Int = letter.charCodeAt(0);

            if ((charCode >= 97 && charCode <= 122) || (charCode >= 48 && charCode <= 57)) {
                keyCode = letter.toUpperCase().charCodeAt(0);
            } else if (charCode == 59) {        // Semi-Colon
                keyCode = Keyboard.SEMICOLON;
            } else if (charCode == 46) {        // Period
                keyCode = Keyboard.PERIOD;
            } else if (charCode == 44) {        // Comma
                keyCode = Keyboard.COMMA;
            } else if (charCode == 47) {        // Divide / Forward-Slash
                keyCode = Keyboard.SLASH;
            }

        }

        gameItem.add(info)
             .add(componentFactory.createCharacter(textField, keyCode))
             .add(componentFactory.createTransform(x, y, rotation))
             .add(componentFactory.createOffset(new Point()))
             .add(componentFactory.createNewton(1, 999.0, vx, vy, torque));

        createSoundEffect("launch");

        engine.addEntity(gameItem);

        return gameItem;
    }

    public function createSoundEffect(soundID:String, loops:Int = 0):Entity {
        var soundEffect:Entity = new Entity();

        soundEffect
                .add(componentFactory.createAudioSource(soundID, "sound", loops))
                .add(componentFactory.createTransient());

        engine.addEntity(soundEffect);

        return soundEffect;
    }

    public function slice(node:GameItemNode):Void {
        var topSlice:Entity = new Entity();
        var bottomSlice:Entity = new Entity();
        var slash:Entity = new Entity();
        var newton:Newton;

        if (node.entity.has(Newton)) {
            newton = node.entity.get(Newton);
        } else {
            throw new Error("EntityFactory::slice() -- No Newton component present");
        }

        var trajectory:Point = node.info.sliceTrajectory.clone();
        var matrix:Matrix = new Matrix();

        matrix.rotate(Math.PI/180 * node.transform.rotation);

        matrix.transformPoint(trajectory);
        trajectory.normalize(8);


        bottomSlice.add(componentFactory.createGameItemInfo(
                node.info.type,
                node.info.category,
                node.info.sliceTrajectory,
                node.info.width,
                node.info.height,
                node.info.splatterColor,
                [node.info.layers[0]]))
            .add(componentFactory.createTransform(node.transform.x, node.transform.y, node.transform.rotation))
            .add(componentFactory.createNewton(1, 999.0, newton.vx , newton.vy, newton.torque, -trajectory.x, -trajectory.y))
            .add(componentFactory.createOffset(new Point()))
            .add(componentFactory.createAlphaTransform(0xFF))
            .add(fruitAlphaOutTween);

        slash.add(componentFactory.createGameItemInfo(
                node.info.type,
                node.info.category,
                node.info.sliceTrajectory,
                node.info.width,
                node.info.height,
                node.info.splatterColor,
                [node.info.layers[0]]))
            .add(componentFactory.createTransform(node.transform.x, node.transform.y, node.transform.rotation))
            .add(componentFactory.createGameItemSlash())
            .add(componentFactory.createAnimation(slashFrames))
            .add(slashTween)
            .add(componentFactory.createTransient(2));

        topSlice.add(componentFactory.createGameItemInfo(
                node.info.type,
                node.info.category,
                node.info.sliceTrajectory,
                node.info.width,
                node.info.height,
                node.info.splatterColor,
                [node.info.layers[1]]))
            .add(componentFactory.createTransform(node.transform.x, node.transform.y, node.transform.rotation))
            .add(componentFactory.createNewton(1, 999.0, newton.vx, newton.vy, newton.torque, trajectory.x, trajectory.y))
            .add(componentFactory.createOffset(new Point()))
            .add(componentFactory.createAlphaTransform(0xFF))
            .add(fruitAlphaOutTween);

        engine.addEntity(bottomSlice);
        engine.addEntity(slash);
        engine.addEntity(topSlice);
    }

    public function createSplatter(node:GameItemNode):Void {
        var splatter:Entity = new Entity();

        var splatterlayer:BitmapLayer = splatterLayers[Math.floor(Math.random() * splatterLayers.length)];
        var info:GameItemInfo = node.info;

        var splatterColor:Int = (info.splatterColor == "")? -1 : Std.parseInt(info.splatterColor);
        splatter
            .add(node.transform)
            .add(componentFactory.createSplatter(splatterColor, splatterlayer))
            .add(componentFactory.createTransient(2.2))
            .add(componentFactory.createAlphaTransform(0xFF))
            .add(splatterAlphaOutTween);

        engine.addEntity(splatter);
    }

    public function createMiss(node:GameItemNode):Entity {
        var miss:Entity = new Entity();

        miss
            .add(componentFactory.createStagger(5))
            .add(componentFactory.createViewportFromRect(new Rectangle(510, 0, 36,36)))
            .add(componentFactory.createTransform(node.transform.x, 378))
            .add(componentFactory.createTransient(0.5));

        createSoundEffect("fruitMissed");

        engine.addEntity(miss);
        return miss;
    }

    public function createComboPopUp(count:Int):Entity {
        var comboPopUp:Entity = new Entity();

        var format:TextFormat = new TextFormat("Garamond Bold", 56, 0xFFFFFF);
        format.align = TextFormatAlign.CENTER;

        var textField:TextField = new TextField();
        textField.embedFonts = true;
        textField.autoSize = TextFieldAutoSize.CENTER;
        textField.antiAliasType = AntiAliasType.ADVANCED;
        textField.gridFitType = GridFitType.PIXEL;
        textField.text = "" + count;
        textField.defaultTextFormat = format;

        comboPopUp
            .add(componentFactory.createTransform(300 + Math.floor(Math.random() * 301), 50 + Math.floor(Math.random() * 51)))
            .add(componentFactory.createPopUp({dx:0, dy:0, name:"combo", bounds:new Rectangle(0, 336, 195, 57)}))
            .add(componentFactory.createCharacter(textField))
            .add(componentFactory.createTransient(3.1))
            .add(componentFactory.createAlphaTransform(0x00))
            .add(popUpAlphaOutTween);

        engine.addEntity(comboPopUp);

        return comboPopUp;
    }

    public function createDoublePointsPopUp():Entity {
        var double:Entity = new Entity();

        double
            .add(componentFactory.createTransform(300 + Math.floor(Math.random() * 301), 50 + Math.floor(Math.random() * 51)))
            .add(componentFactory.createPopUp({dx:0, dy:0, name:"doublePoints", bounds:new Rectangle(0, 287, 169, 49)}))
            .add(componentFactory.createTransient(3.1))
            .add(componentFactory.createAlphaTransform(0x00))
            .add(popUpAlphaOutTween);

        engine.addEntity(double);

        return double;
    }

    public function createMenuButton(name:String, layers:Array<BitmapLayer>, bounds:Rectangle):Entity {
        var button:Entity = new Entity(name);
        var mirror:Entity = new Entity(name + ".mirror");
        var slash:Entity = new Entity();

        var viewport:Viewport = componentFactory.createViewportFromRect(bounds);
        var btnInfo:MenuButtonInfo = componentFactory.createMenuButtonInfo(name, layers);

        var pivot:Vector3D = new Vector3D(
            bounds.left + ((bounds.right - bounds.left) * 0.5),
            bounds.top + ((bounds.bottom - bounds.top) * 0.5)
        );

        var points:Vector<Float> = Vector.fromArray(
            [
                bounds.left, bounds.top, 0,
                bounds.right, bounds.top, 0,
                bounds.right, bounds.bottom, 0,
                bounds.left, bounds.bottom, 0
            ]
        );

        button
            .add(btnInfo)
            .add(componentFactory.createTransform())
            .add(viewport);

        mirror
            .add(componentFactory.createTransformVector(points, pivot))
            .add(componentFactory.createTransform())
            .add(viewport);

        engine.addEntity(button);
        engine.addEntity(mirror);

        return button;
    }

    public function sliceMenuButton(mirror:Entity):Void {
        var viewport:Viewport = mirror.get(Viewport);
        var slash:Entity = new Entity();


        // calculate slash anim bounds in the scene bitmap's co-ordinate space
        slash
            .add(componentFactory.createViewportFromRect(new Rectangle(
                ((viewport.rect.width - slashFrames[0].width) * 0.5) + viewport.rect.left,
                viewport.rect.top + 4,
                slashFrames[0].width,
                slashFrames[0].height)))
            .add(componentFactory.createAnimation(slashFrames))
            .add(slashTween)
            .add(componentFactory.createTransient(2))
            .add(componentFactory.createMenuSlash());

        Actuate.timer(0.2).onComplete(dropSign, [mirror]);

        engine.addEntity(slash);
    }

    public function dropSign(mirror:Entity):Void {
        createSoundEffect("swordSlash");

        mirror
        .add(componentFactory.createViewportTransform(0.6))
        .add(componentFactory.createNewton(1, 999.0, 0, 8, -1));
    }

    public function explode(node:GameItemNode):Void {
        var explosion:Entity = new Entity();
        var bloom:Entity = new Entity();

        var bomb:Entity = node.entity;

        createSoundEffect("explode");
        bomb.remove(Newton);
        bomb.remove(Character);
        bomb.add(componentFactory.createTransient(0.5));

        var cut:BitmapData = new BitmapData(916, 400, true, 0x00000000);
        cut.copyPixels(
            fillAlpha,
            new Rectangle(916 - node.transform.x, 400 - node.transform.y, 916, 400),
            new Point()
        );

        explosion
            .add(node.info)
            .add(node.transform)
            .add(componentFactory.createColorTransformWrapper())
            .add(componentFactory.createBloomEffect(cut))
            .add(componentFactory.createAnimation(explodeFrames))
            .add(componentFactory.createGameItemExplosion())
            .add(componentFactory.createTransient(2.0))
            .add(explodeTween);

        engine.addEntity(explosion);
    }
}