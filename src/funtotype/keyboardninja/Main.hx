package funtotype.keyboardninja;

import openfl.events.Event;
import openfl.display.StageScaleMode;
import openfl.display.Sprite;

class Main extends Sprite {

    public function new() {
        super();
        stage.scaleMode = StageScaleMode.NO_SCALE;
        addEventListener(Event.ENTER_FRAME, onEnterFrame);
        this.name = "Main";

    }

    private function onEnterFrame(event:Event):Void {
        removeEventListener(Event.ENTER_FRAME, onEnterFrame);

        var game:KeyboardNinjaGame = new KeyboardNinjaGame(this);
        game.start();
    }
}
