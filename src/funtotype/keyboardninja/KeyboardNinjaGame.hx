package funtotype.keyboardninja;

import ash.systems.UpdateAnimationSystem;
import funtotype.systems.DrawMenuSlashSystem;
import ash.systems.ViewportTransformSystem;
import ash.components.ViewportTransform;
import funtotype.systems.SliceButtonSystem;
import openfl.geom.ColorTransform;
import ash.systems.UpdateBitmapPlaneViewportSystem;
import ash.core.System;
import ash.core.Entity;
import ash.core.Engine;

import ash.components.FSMInfo;
import ash.components.Transform2D;
import ash.components.BitmapSource;

import ash.tick.FrameTickProvider;
import ash.factories.ComponentFactory;

import ash.interfaces.INetForceSource;

import ash.systems.AudioSystem;
import ash.systems.TransientSystem;
import ash.systems.ForceResolverSystem;
import ash.systems.FSMTransitionSystem;
import ash.systems.ComponentTweenSystem;
import ash.systems.ViewStateSelectSystem;
import ash.systems.RenderBitmapPlaneSystem;

import typedefs.Layer;
import typedefs.TileInfo;
import typedefs.BitmapLayer;
import typedefs.TileMetaData;
import typedefs.ViewContext;

import openfl.Assets;
import openfl.geom.Point;
import openfl.geom.Rectangle;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Tilesheet;
import openfl.display.PixelSnapping;
import openfl.display.BitmapDataChannel;
import openfl.display.DisplayObjectContainer;

import net.richardlord.input.KeyPoll;
import net.richardlord.enums.SystemPriorities;

import funtotype.enums.GameItemCategory;

import funtotype.components.GameConfig;
import funtotype.components.GameItemInfo;

import funtotype.factories.EntityFactory;

import funtotype.systems.PopUpSystem;
import funtotype.systems.StaggerSystem;
import funtotype.systems.SplatterSystem;
import funtotype.systems.IceVignetteSystem;
import funtotype.systems.ClearCanvasSystem;
import funtotype.systems.GameItemSpawnSystem;
import funtotype.systems.MenuButtonCompositor;
import funtotype.systems.GameSessionController;
import funtotype.systems.KeyPollProcessorSystem;
import funtotype.systems.ResetGameSessionSystem;
import funtotype.systems.GameHUDCompositorSystem;
import funtotype.systems.GameSessionReportSystem;
import funtotype.systems.GameItemCompositorSystem;

import funtotype.utils.Compositor;

import utils.PointColorCache;

import haxe.Json;
import Std.int;
import EReg;

class KeyboardNinjaGame {
    private var engine:Engine;
    private var keyPoll:KeyPoll;
    private var tickProvider:FrameTickProvider;
    private var container:DisplayObjectContainer;
    private var bitmapContainer:Bitmap;

    private var entityFactory:EntityFactory;
    private var componentFactory:ComponentFactory;

    private var canvas:BitmapData;
    private var scene:BitmapData;
    private var btnBuffer:BitmapData;
    private var menu:BitmapData;

    private var metadata:Array<TileMetaData>;
    private var spritesheet:BitmapData;
    private var tilesheet:Tilesheet;

    private var colorMap:Map<Int, String>;
    private var viewportBounds:Rectangle;
    private var backgroundBounds:Rectangle;

    private var colorCache:PointColorCache;
    private var gameItemData:Array<GameItemInfo>;
    private var netForceSource:INetForceSource;

    private var splatterLayers:Array<BitmapLayer>;
    private var compositor:Compositor;

    public function new(container:DisplayObjectContainer) {
        this.container = container;

        engine = new Engine();
        keyPoll = new KeyPoll(container.stage);

        viewportBounds = new Rectangle(916, 0, 916, 400);

        colorMap = new Map<Int, String>();

        metadata = Json.parse(Assets.getText("sheetData"));
        spritesheet = Assets.getBitmapData("images/spritesheet.png");
        tilesheet = new Tilesheet(spritesheet);

        splatterLayers = [];
        componentFactory = new ComponentFactory();

        tickProvider = new FrameTickProvider(container);
        tickProvider.add(engine.update);

        prepareAssets();
        createEntities();
        initializeSystems();
    }

    private function prepareAssets():Void {
        var id:Int;
        var info:TileInfo;
        var rect:Rectangle;
        var center:Point;
        var map:Map<String, TileInfo> = new Map<String, TileInfo>();

        // Partition tilesheet
        for (t in metadata) {
            rect = new Rectangle(t.x, t.y, t.width, t.height);
            center = (t.centroid.length > 0)? new Point(t.centroid[0], t.centroid[1]): new Point();
            id = tilesheet.addTileRect(rect, center);
            map.set(t.name, {id:id, center:center, rect:rect});

            if (t.name.indexOf("splatterMask") != -1) {
                splatterLayers.push({dx:0, dy:0, name:t.name, bounds:rect.clone()});
            }

        }

        // Create entity factory
        entityFactory  = new EntityFactory(engine, componentFactory, splatterLayers);

        // Generate game item data
        var gameItems:Array<Dynamic> = Json.parse(Assets.getText("gameitems"));

        var top:Rectangle;
        var bottom:Rectangle;

        gameItemData = new Array<GameItemInfo>();

        for (item in gameItems) {

            if ((info = map.get(item.name + "Top")) == null) {
                top = bottom = map.get(item.name).rect.clone();
            } else {
                top = info.rect.clone();
                bottom = map.get(item.name + "Bottom").rect.clone();
            }

            gameItemData.push(
                componentFactory.createGameItemInfo(
                    item.name,
                    Type.createEnum(GameItemCategory, item.category),
                    Point.polar(1, Math.PI/180 * item.orientation),
                    top.width,
                    top.height,
                    item.splatterColor,
                    [
                        {dx:0, dy:0, name:"bottom", bounds: bottom},
                        {dx:0, dy:0, name:"top", bounds: top}
                    ]
                )
            );
        }

        // Generate scene data
        var layers:Array<Layer> = Json.parse(Assets.getText("viewData")),
            root:Layer,
            tiles:Array<Layer>;

        root = layers.filter(
            function(item:Layer):Bool {
                return item.name == "root";
            }
        )[0];

        tiles = layers.filter(
            function(item:Layer):Bool {
                return (item.children.length == 0 && item.parent.indexOf("buttons") == -1);
            }
        );

        tiles.reverse();

        // create scene bitmap
        scene = new BitmapData(int(root.width), int(root.height), true, 0x00000000);

        for (t in tiles) {
            info = map.get(t.name.split('_')[0]);
            id = info.id;//map.get(t.name.split('_')[0]);

            rect = tilesheet.getTileRect(id);
            scene.copyPixels(spritesheet, rect, new Point(t.x, t.y), null, null, true);
        }

        // create backbuffer for color picking and create menuButton entities
        var btnName:String,
            buttonGroupLayers:Array<Layer>,
            buttonGroupLayerPattern:EReg = ~/([menu]|[game])\..+.buttons\./,
            r:Int,
            g:Int,
            b:Int,
            view:Rectangle,
            childLayer:Layer,
            children:Array<String>,
            bmpLayers:Array<BitmapLayer>;

        btnBuffer = new BitmapData(int(root.width), int(root.height), true, 0x00000000);

        buttonGroupLayers = layers.filter(
            function(item:Layer):Bool {
                return buttonGroupLayerPattern.match(item.name);
            }
        );

        // create a menuButton entity for each group layer
        for (groupLayer in buttonGroupLayers) {
            view = new Rectangle(groupLayer.x, groupLayer.y, groupLayer.width, groupLayer.height);
            bmpLayers = new Array<BitmapLayer>();

            children = groupLayer.children;

            while(groupLayer.children.length > 0) {

                btnName = children.shift();

                childLayer = layers.filter(
                    function(item:Layer):Bool {
                        return item.name == btnName;
                    }
                )[0];

                id = map.get(btnName.split('_')[0]).id;
                rect = tilesheet.getTileRect(id);

                bmpLayers.unshift(
                    {
                        dx:childLayer.x - groupLayer.x,     // Offsets relative to
                        dy:childLayer.y - groupLayer.y,     // parent group layer

                        name:childLayer.name,               // child's bitmap area in
                        bounds: rect                        // spritesheet
                    }
                );

            }

            entityFactory.createMenuButton(groupLayer.name, bmpLayers, view);

            do {
                r = int(Math.random() * 256) << 16;
                g = int(Math.random() * 256) << 8;
                b = int(Math.random() * 256);

            } while(colorMap.exists(r+g+b));

            colorMap.set(0xFF000000 | r | g | b, groupLayer.name);

            btnBuffer.fillRect(new Rectangle(childLayer.x, childLayer.y, rect.width, rect.height), 0xFF000000 | r | g | b);
            btnBuffer.copyChannel(spritesheet, rect, new Point(childLayer.x, childLayer.y), BitmapDataChannel.ALPHA, BitmapDataChannel.ALPHA);
        }

        backgroundBounds = map.get("background").rect;

        canvas = new BitmapData(916, 400, true, 0x00000000);
        menu = new BitmapData(916, 400, true, 0x00000000);

        bitmapContainer = new Bitmap(canvas, PixelSnapping.ALWAYS, false);
        container.addChild(bitmapContainer);

        colorCache = new PointColorCache(container.stage, btnBuffer, viewportBounds, colorMap);

        canvas.copyPixels(scene, new Rectangle(916, 400, 916, 400), new Point());
        canvas.colorTransform(new Rectangle(0, 0, 916, 400), new ColorTransform(0, 0, 0, 0, 31, 64, 64, 128));

        scene.copyPixels(canvas, canvas.rect, new Point(916, 400), null, null, true);

        canvas.fillRect(canvas.rect, 0x00000000);

        compositor = new Compositor(spritesheet);
        compositor.addTarget("sceneMap", scene);
        compositor.addTarget("menuMap", menu);
    }

    private function createEntities():Void {
        var topLeft:Point = new Point();

        // Create background render plane
        entityFactory.createRenderPlane("background", spritesheet, backgroundBounds, topLeft);

        // Load view context data
        var context:ViewContext = Json.parse(Assets.getText("viewContext"));

        // Create render plane for in-game shenanigans
        var _fruits:Entity =  entityFactory.createRenderPlane("gameItemCanvas", scene, new Rectangle(2748, 800, 916, 400), topLeft);

        // Create iceVignette entity
        var _iceVignette:Entity = entityFactory.createIceVignette("iceVignette", scene, new Rectangle(916, 400, 916, 400), topLeft);

        // Create freeze state render plane
        var _iceCanvasBMD:BitmapData = new BitmapData(916, 400, true, 0x00000000);
        var _iceCanvas:Entity = entityFactory.createRenderPlane(
            "iceCanvas",
            _iceCanvasBMD,
            _iceCanvasBMD.rect,
            topLeft
        );

        // Create scene render plane and add view FSM
        var _scene:Entity = entityFactory.createRenderPlane("scene", scene, viewportBounds, topLeft);
        _scene.get(Transform2D).invalidate = true;

        entityFactory.addViewFSM(_scene, context);

        // Create menu button render plane
        var _menu:Entity = entityFactory.createRenderPlane("menu", menu, new Rectangle(0, 0, 916, 400), topLeft);

        // Create backbuffer for color-based picking; Note: backbuffer viewport shares the same rectangle instance as
        // the scene render plane's viewport.
        entityFactory.createButtonBackBuffer("backBuffer", _scene.get(FSMInfo));

        // Create Game state entity
        var _gameState:Entity = entityFactory.createGameSession(_scene.get(FSMInfo));
        netForceSource = _gameState.get(GameConfig);
    }

    /**
     * Initialize Systems.
     **/
    private function initializeSystems():Void {

        // Create state lookup
        var stateMap:Map<String, String> = new Map<String, String>();

        stateMap.set("menu.howTo.buttons.backButton", "titleNoMusic");
        stateMap.set("menu.title.buttons.howToPlayButton", "howTo");
        stateMap.set("menu.title.buttons.playGameButton", "selectLesson");
        stateMap.set("menu.selectLesson.buttons.numberPadButton", "selectDifficulty");
        stateMap.set("menu.selectLesson.buttons.topRowButton", "selectDifficulty");
        stateMap.set("menu.selectLesson.buttons.bottomRowButton", "selectDifficulty");
        stateMap.set("menu.selectLesson.buttons.homeRowButton", "selectDifficulty");
        stateMap.set("menu.selectLesson.buttons.allLettersButton", "selectDifficulty");
        stateMap.set("menu.selectDifficulty.buttons.easyButton", "game");
        stateMap.set("menu.selectDifficulty.buttons.mediumButton", "game");
        stateMap.set("menu.selectDifficulty.buttons.hardButton", "game");
        stateMap.set("game.over.buttons.playAgainButton", "gameReplay");
        stateMap.set("game.over.buttons.mainMenuButton", "reset");

        engine.addSystem(new GameSessionController(entityFactory, keyPoll, colorCache), SystemPriorities.preUpdate);

        engine.addSystem(new ViewStateSelectSystem(colorCache, stateMap), SystemPriorities.preUpdate);
        engine.addSystem(new FSMTransitionSystem(), SystemPriorities.update);

        engine.addSystem(new ComponentTweenSystem(), SystemPriorities.update);

        engine.addSystem(new GameItemSpawnSystem(entityFactory, gameItemData), SystemPriorities.update);
        engine.addSystem(new KeyPollProcessorSystem(keyPoll, entityFactory), SystemPriorities.update);
        engine.addSystem(new ClearCanvasSystem(compositor), SystemPriorities.update);
        engine.addSystem(new SplatterSystem(compositor), SystemPriorities.update);
        engine.addSystem(new GameItemCompositorSystem(spritesheet, entityFactory, compositor), SystemPriorities.update);
        engine.addSystem(new PopUpSystem(compositor), SystemPriorities.update);
        engine.addSystem(new StaggerSystem(compositor), SystemPriorities.update);
        engine.addSystem(new GameHUDCompositorSystem(scene), SystemPriorities.update);
        engine.addSystem(new GameSessionReportSystem(spritesheet, scene), SystemPriorities.update);

        engine.addSystem(new AudioSystem(), SystemPriorities.update);
        engine.addSystem(new TransientSystem(), SystemPriorities.update);
        engine.addSystem(new IceVignetteSystem(), SystemPriorities.update);
        engine.addSystem(new UpdateBitmapPlaneViewportSystem(), SystemPriorities.update);
        engine.addSystem(new SliceButtonSystem(colorCache, entityFactory), SystemPriorities.update);
        engine.addSystem(new ForceResolverSystem(netForceSource), SystemPriorities.update);
        engine.addSystem(new ViewportTransformSystem(), SystemPriorities.update);
        engine.addSystem(new MenuButtonCompositor(compositor), SystemPriorities.update);
        engine.addSystem(new UpdateAnimationSystem(), SystemPriorities.update);
        engine.addSystem(new DrawMenuSlashSystem(compositor), SystemPriorities.update);
        engine.addSystem(new RenderBitmapPlaneSystem(canvas), SystemPriorities.update);
        engine.addSystem(new ResetGameSessionSystem(), SystemPriorities.update);
    }

    public function start():Void {
        tickProvider.start();
    }
}