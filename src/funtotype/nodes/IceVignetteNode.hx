package funtotype.nodes;

import ash.components.AlphaTransform;
import ash.core.Node;

import ash.components.Offset;
import ash.components.Viewport;
import ash.components.BitmapSource;

import funtotype.components.IceVignetteTag;

class IceVignetteNode extends Node<IceVignetteNode>{
    public var tag:IceVignetteTag;
    public var source:BitmapSource;
    public var dest:Offset;
    public var aTransform:AlphaTransform;
    public var viewport:Viewport;

    public function new() {}
}
