package funtotype.nodes;

import ash.core.Node;
import funtotype.components.ResetGameSession;

class ResetGameSessionNode extends Node<ResetGameSessionNode> {
    public var reset:ResetGameSession;

    public function new() {}
}
