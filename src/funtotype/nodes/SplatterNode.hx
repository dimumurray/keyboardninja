package funtotype.nodes;

import ash.core.Node;
import ash.components.Offset;
import ash.components.Transform2D;
import ash.components.AlphaTransform;

import funtotype.components.Splatter;

class SplatterNode extends Node<SplatterNode>{
    public var splatter:Splatter;
    public var transform:Transform2D;
    public var aTransform:AlphaTransform;

    public function new() {}
}
