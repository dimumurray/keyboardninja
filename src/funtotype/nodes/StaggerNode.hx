package funtotype.nodes;

import ash.core.Node;

import ash.components.Viewport;
import ash.components.Transform2D;

import funtotype.components.Stagger;

class StaggerNode extends Node<StaggerNode> {
    public var stagger:Stagger;
    public var viewport:Viewport;
    public var transform:Transform2D;

    public function new() {}
}
