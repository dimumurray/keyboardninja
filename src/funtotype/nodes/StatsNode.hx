package funtotype.nodes;

import ash.components.FSMInfo;
import ash.core.Node;
import ash.components.BitmapSource;

import funtotype.components.GameStats;

class StatsNode extends Node<StatsNode> {
    public var stats:GameStats;
    public var bitmap:BitmapSource;
    public var fsm:FSMInfo;

    public function new() {}

    public function reset() {
        stats.pointTotal = 0;
        stats.multiplier = 1;
        stats.invalidate = false;
    }

    public function setMultiplier(v:Int):Void {
        stats.multiplier = v;
    }

    public function increment():Void {
        stats.pointTotal += stats.pointGain * stats.multiplier;
        stats.invalidate = true;
    }

    public function decrement():Void {
        stats.pointTotal -= stats.pointLoss;
        stats.invalidate = true;
    }
}
