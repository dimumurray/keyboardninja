package funtotype.nodes;

import ash.core.Node;

import ash.components.Viewport;
import ash.components.Transform2D;

import funtotype.components.MenuButtonInfo;

class MenuButtonNode extends Node<MenuButtonNode> {
    public var info:MenuButtonInfo;
    public var viewport:Viewport;
    public var transform:Transform2D;

    public function new() {}
}
