package funtotype.nodes;

import ash.core.Node;
import ash.components.Transform2D;

import funtotype.components.Character;
import funtotype.components.GameItemInfo;

class GameItemNode extends Node<GameItemNode> {
    public var info:GameItemInfo;
    public var transform:Transform2D;

    public function new() {}

    public function getKeyCode():Int {
        if(entity.has(Character)) {
            return entity.get(Character).keyCode;
        }

        return -1;
    }
}
