package funtotype.nodes;

import ash.core.Node;
import funtotype.components.GameSessionReport;

class GameSessionReportNode extends Node<GameSessionReportNode> {
    public var report:GameSessionReport;

    public function new() {}
}
