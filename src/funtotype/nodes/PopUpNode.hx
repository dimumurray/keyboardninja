package funtotype.nodes;

import ash.core.Node;
import ash.components.Offset;
import ash.components.Transform2D;

import funtotype.components.PopUp;
import funtotype.components.Character;

class PopUpNode extends Node<PopUpNode>{
    public var popUp:PopUp;
    public var transform:Transform2D;

    public function new() {}
}
