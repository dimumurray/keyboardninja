package funtotype.nodes;

import ash.core.Node;

import ash.components.FSMInfo;
import ash.components.AudioSettings;

import funtotype.components.GameStats;
import funtotype.components.Health;
import funtotype.components.GameConfig;


class GameSessionNode extends Node<GameSessionNode> {
    public var stats:GameStats;
    public var health:Health;
    public var config:GameConfig;
    public var audioSettings:AudioSettings;
    public var fsmInfo:FSMInfo;

    public function new() {}
}
