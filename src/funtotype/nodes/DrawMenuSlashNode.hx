package funtotype.nodes;

import ash.core.Node;
import ash.components.Animation;
import ash.components.Viewport;

import funtotype.components.MenuSlash;

class DrawMenuSlashNode extends Node<DrawMenuSlashNode>{
    public var anim:Animation;
    public var slash:MenuSlash;
    public var viewport:Viewport;

    public function new() {
    }
}
