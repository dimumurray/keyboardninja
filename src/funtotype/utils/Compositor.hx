package funtotype.utils;

import ash.components.Animation;
import openfl.display.DisplayObject;
import openfl.geom.ColorTransform;
import openfl.geom.Rectangle;
import ash.components.Transform2D;
import ash.components.AlphaTransform;

import openfl.geom.Point;
import openfl.geom.Matrix;
import openfl.geom.Rectangle;
import openfl.text.TextField;
import openfl.display.BitmapData;
import openfl.display.BitmapDataChannel;

import typedefs.BitmapLayer;

import Std.int;

class Compositor {
    private var source:BitmapData;
    private var target:BitmapData;

    private var scratch:BitmapData;
    private var temp:BitmapData;

    private var matrix:Matrix;

    private var halfDim:Float;
    private var PI_DIV_180:Float;

    private var defaultPoint:Point;

    private var alphaColorTransform:ColorTransform;
    private var rgbChannelColorTransform:ColorTransform;

    private var points:Array<Point>;
    private var targetMap:Map<String, BitmapData>;

    public function new(source:BitmapData) {
        this.source = source;

        targetMap = new Map<String, BitmapData>();

        matrix = new Matrix();

        temp = new BitmapData(256, 256, true, 0x00000000);

        halfDim = 128;
        PI_DIV_180 = Math.PI / 180;

        defaultPoint = new Point(0,0);

        alphaColorTransform = new ColorTransform();
        rgbChannelColorTransform = new ColorTransform();

        points = new Array<Point>();

        for (i in 0...4) {
            points.push(new Point());
        }

    }

    public function addTarget(name:String, target:BitmapData):Void {
        targetMap.set(name, target);
    }

    public function setCurrentTarget(name:String):Void {
        target = targetMap.get(name);
    }

    public function clearTargetRegion(rect:Rectangle):Void {
        var clear:BitmapData = new BitmapData(int(rect.width), int(rect.height), true, 0x00000000);
        target.copyPixels(clear, clear.rect, rect.topLeft);
        clear.dispose();
    }

    public function drawToCanvas(rect:Rectangle, offset:Point):Void {
        target.copyPixels(source, rect, offset, null, null, true);
    }

    public function drawBitmapToCanvas(bitmap:BitmapData, offset:Point, colorTransform:ColorTransform = null):Void {
        if (colorTransform != null) {
            bitmap.colorTransform(bitmap.rect, colorTransform);
        }

        target.copyPixels(bitmap, bitmap.rect, offset, null, null, true);
    }

    public function drawFrameToCanvas(frame:DisplayObject, offset:Point):Void {
        var copy:BitmapData = new BitmapData(int(frame.width), int(frame.height), true, 0x00000000);
        copy.draw(frame);

        target.copyPixels(copy, copy.rect, offset, null, null, true);

        copy.dispose();
    }

    public function drawCurrentFrameToFullCanvas(anim:Animation, transform:Transform2D, offset:Point):Void {
        var currentFrame:DisplayObject = anim.frames[anim.currentFrame];

        matrix.identity();
        matrix.translate(transform.x + offset.x, transform.y + offset.y);

        target.draw(currentFrame, matrix);
    }

    public function drawCurrentFrameToScratch(anim:Animation):Void {
        var currentFrame:DisplayObject = anim.frames[anim.currentFrame];

        scratch = new BitmapData(int(currentFrame.width), int(currentFrame.height), true, 0x00000000);
        scratch.draw(currentFrame);

        setPoints(scratch.rect);
    }

    public function drawLayersToScratch(layers:Array<BitmapLayer>):Void {
        // Assumes that all layers in array have the same dimensions
        var bounds:Rectangle = layers[0].bounds;

        scratch = new BitmapData(int(bounds.width), int(bounds.height), true, 0x00000000);

        for (layer in layers) {
            scratch.copyPixels(
                source,
                layer.bounds,
                new Point(layer.dx, layer.dy),
                null,
                null,
                true);
        }

        setPoints(scratch.rect);
    }

    public function drawFillAlphaToScratch(color:Int, rect:Rectangle):Void {
        rgbChannelColorTransform.color = color;
        rgbChannelColorTransform.alphaMultiplier = 1;
        rgbChannelColorTransform.alphaOffset = 0;

        scratch = new BitmapData(int(rect.width), int(rect.height), true, 0x00000000);
        scratch.copyPixels(source, rect, defaultPoint);
        scratch.colorTransform(scratch.rect, rgbChannelColorTransform);

        setPoints(scratch.rect);
    }

    private function setPoints(bounds:Rectangle):Void {
        points[0].x = bounds.left;
        points[0].y = bounds.top;

        points[1].x = bounds.right;
        points[1].y = bounds.top;

        points[2].x = bounds.right;
        points[2].y = bounds.bottom;

        points[3].x = bounds.left;
        points[3].y = bounds.bottom;
    }

    public function applyAlphaTransform(aTransform:AlphaTransform):Void {
        if(aTransform == null) return;

        temp = new BitmapData(scratch.width, scratch.height, true, 0x00000000);

        scratch.merge(temp, temp.rect, defaultPoint, 1, 1, 1, 0xFF - aTransform.alpha);

    }

    public function drawTransformedToCanvas(transform:Transform2D, offset:Point, center:Bool = false):Void {
        matrix.identity();

       if (transform.rotation != 0 ){
            var dx:Float = scratch.width * .5;
            var dy:Float = scratch.height * .5;

            matrix.translate(-dx, -dy);
            matrix.rotate(PI_DIV_180 * transform.rotation);

            var minX:Float = 999;
            var minY:Float = 999;
            var maxX:Float = 0;
            var maxY:Float = 0;

            for (i in 0...4) {
                points[i] = matrix.transformPoint(points[i]);

                minX = Math.min(minX, points[i].x);
                maxX = Math.max(maxX, points[i].x);
                minY = Math.min(minY, points[i].y);
                maxY = Math.max(maxY, points[i].y);

            }

            var width:Int = Math.ceil(maxX - minX);
            var height:Int = Math.ceil(maxY - minY);

            matrix.translate(-minX, -minY);

            temp = new BitmapData(width, height, true, 0x00000000);
            temp.draw(scratch, matrix);

        } else {
            temp = scratch;
        }

        var o:Point = offset.clone();
        o.offset(transform.x - ((center)?(temp.width * 0.5):0), transform.y - ((center)?(temp.height * 0.5):0));

        target.copyPixels(temp, temp.rect, o, null, null, true);

        scratch.dispose();
        temp.dispose();
    }

    public function drawTextField(textField:TextField, transform:Transform2D, offset:Point, center:Bool = false):Void {

        temp = new BitmapData(Math.ceil(textField.width), Math.ceil(textField.height), true, 0x00000000);

        var o:Point = offset.clone();
        o.offset(transform.x - ((center)?(temp.width * 0.5):0) + textField.x, transform.y - ((center)?(temp.height * 0.5):0) + textField.y);

        temp.draw(textField);
        target.copyPixels(temp, temp.rect, o, null, null, true);
        temp.dispose();
    }
}
