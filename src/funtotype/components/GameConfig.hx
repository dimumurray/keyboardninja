package funtotype.components;

import ash.interfaces.INetForceSource;
import funtotype.enums.Difficulty;
import funtotype.enums.LessonType;

class GameConfig implements INetForceSource {
    public var difficulty:Difficulty;
    public var lessonType:LessonType;

    public var running:Bool = false;

    public var characterList:Array<String>;

    public var fruitPerWave:Int;
    public var bombChanceFactor:Int;

    public var speed:Float = 6;
    public var gravity:Float = .270;

    public var timeScale:Float = 0.5;

    public var timeScaleMultiplier:Float = 1.0;

    public function new() {}

    public function getNetForce():Float {
        return gravity * (timeScale * timeScaleMultiplier);
    }

    public function getTimeScale():Float {
        return timeScale * timeScaleMultiplier;
    }
}
