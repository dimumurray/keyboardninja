package funtotype.components;

class GameStats {
    public var pointTotal:Int = 0;
    public var pointGain:Int = 13;
    public var pointLoss:Int = -7;

    public var gauge:Int = 0;

    public var timePlayed:Float = 0;
    public var fruitsSliced:Int = 0;

    public var multiplier:Int = 1;

    public var invalidate:Bool = false;

    public function new() {
    }
}
