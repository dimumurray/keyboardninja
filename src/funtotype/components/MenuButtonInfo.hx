package funtotype.components;

import typedefs.BitmapLayer;

class MenuButtonInfo {
    public var name:String;
    public var layers:Array<BitmapLayer>;

    public function new() {}
}
