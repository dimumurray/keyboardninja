package funtotype.components;

import funtotype.enums.GameItemCategory;
import typedefs.BitmapLayer;

import openfl.geom.Point;

class GameItemInfo {
    public var type:String;
    public var category:GameItemCategory;
    public var sliceTrajectory:Point;
    public var width:Float;
    public var height:Float;
    public var generation:Int;
    public var splatterColor:String;
    public var layers:Array<BitmapLayer>;

    public function new() {}
}
