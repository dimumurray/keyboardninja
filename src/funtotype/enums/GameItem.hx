package funtotype.enums;

enum GameItem {
    redApple;
    greenApple;
    lemon;
    coconut;
    banana;
    kiwi;
    pomegranate;
    orange;
    bomb;
    ice;
    pineapple;
    cherry;
}