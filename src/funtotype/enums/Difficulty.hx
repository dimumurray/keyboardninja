package funtotype.enums;

enum Difficulty {
    EASY;
    MEDIUM;
    HARD;
}