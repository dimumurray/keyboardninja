package funtotype.enums;

enum GameItemCategory {
    BUFF;
    NERF;
    NEUTRAL;
    SLICED;
    SPLATTERED;
}