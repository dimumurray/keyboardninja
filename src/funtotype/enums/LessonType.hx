package funtotype.enums;

enum LessonType {
    ALL_LETTERS;
    HOME_ROW;
    TOP_ROW;
    BOTTOM_ROW;
    NUMBERS;
}