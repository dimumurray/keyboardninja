package funtotype.systems;

import ash.components.TransformVector;
import ash.components.ViewportTransform;
import ash.components.Newton;
import ash.components.Transform2D;
import ash.core.Entity;
import ash.core.System;
import ash.core.Engine;
import ash.core.NodeList;

import ash.components.Viewport;
import ash.components.BitmapSource;
import ash.components.AlphaTransform;

import funtotype.utils.Compositor;
import funtotype.nodes.MenuButtonNode;
import funtotype.components.GameConfig;

import openfl.Vector;
import openfl.geom.Point;
import openfl.geom.Rectangle;

class MenuButtonCompositor extends System {
    private var nodes:NodeList<MenuButtonNode>;
    private var sceneViewport:Rectangle;
    private var sceneTransform:Transform2D;
    private var config:GameConfig;
    private var engine:Engine;

    private var canvas:BitmapSource;
    private var compositor:Compositor;

    private var topLeft:Point;

    private var mirror:Entity;
    private var mirrorTransformMap:Map<MenuButtonNode, Transform2D>;
    private var mirrorPointsMap:Map<MenuButtonNode, Vector<Float>>;

    private var mirrorTransform:Transform2D;
    private var mirrorPoints:Vector<Float>;

    public function new(compositor:Compositor) {
        super();
        this.compositor = compositor;

        mirrorPointsMap = new Map<MenuButtonNode, Vector<Float>>();
        mirrorTransformMap = new Map<MenuButtonNode, Transform2D>();
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        this.engine = engine;

        nodes = engine.getNodeList(MenuButtonNode);

        config = engine.getEntityByName("gameSession").get(GameConfig);

        sceneViewport = engine.getEntityByName("scene").get(Viewport).rect;
        sceneTransform = engine.getEntityByName("scene").get(Transform2D);
        canvas = engine.getEntityByName("menu").get(BitmapSource);
        topLeft = canvas.bitmap.rect.topLeft;

        for (node in nodes) {
            onNodeAdded(node);
        }

        nodes.nodeAdded.add(onNodeAdded);

    }

    private function onNodeAdded(node:MenuButtonNode):Void {
        mirror = engine.getEntityByName(node.info.name + ".mirror");

        mirrorTransformMap.set(node, mirror.get(Transform2D));
        mirrorPointsMap.set(node, mirror.get(TransformVector).points);
    }

    override public function update(time:Float):Void {
        compositor.setCurrentTarget("menuMap");
        compositor.clearTargetRegion(canvas.bitmap.rect);

        canvas.hasContentToRender = false;
        canvas.invalidate = sceneTransform.invalidate;

        for (node in nodes) {
            mirror = engine.getEntityByName(node.info.name + ".mirror");

            if (sceneViewport.containsRect(node.viewport.rect) || sceneViewport.intersects(node.viewport.rect)) {
                compositor.drawLayersToScratch(node.info.layers);
                compositor.applyAlphaTransform(node.entity.get(AlphaTransform));

                mirrorTransform = mirrorTransformMap.get(node);
                mirrorPoints = mirrorPointsMap.get(node);

                node.transform.x = mirrorTransform.x + mirrorPoints[0] - sceneViewport.x;
                node.transform.y = mirrorTransform.y + mirrorPoints[1] - sceneViewport.y;
                node.transform.rotation = mirrorTransform.rotation;

                compositor.drawTransformedToCanvas(node.transform, topLeft);

                canvas.hasContentToRender = true;
            } else {

                if(mirror != null && mirror.has(Newton) && mirror.has(ViewportTransform)) {
                    mirror.remove(Newton);
                    mirror.remove(ViewportTransform);
                }

            }

        }

    }

}
