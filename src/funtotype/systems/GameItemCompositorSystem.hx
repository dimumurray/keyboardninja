package funtotype.systems;

import ash.core.Entity;
import ash.core.Engine;
import ash.core.System;
import ash.core.NodeList;

import ash.components.Viewport;
import ash.components.Animation;
import ash.components.BitmapSource;
import ash.components.AlphaTransform;
import ash.components.ColorTransformWrapper;

import funtotype.components.Health;
import funtotype.components.Character;
import funtotype.components.GameConfig;
import funtotype.components.BloomEffect;
import funtotype.components.GameItemInfo;
import funtotype.components.GameItemSlash;
import funtotype.components.GameItemExplosion;

import funtotype.nodes.GameItemNode;
import funtotype.enums.GameItemCategory;
import funtotype.factories.EntityFactory;
import funtotype.utils.Compositor;

import openfl.geom.Point;
import openfl.geom.Rectangle;
import openfl.display.BitmapData;

class GameItemCompositorSystem extends System {
    private var spriteSheet:BitmapData;
    private var nodes:NodeList<GameItemNode>;

    private var config:GameConfig;
    private var health:Health;

    private var topLeft:Point;
    private var bottomRight:Point;
    private var canvasRect:Rectangle;
    private var canvas:BitmapSource;

    private var engine:Engine;

    private var factory:EntityFactory;

    private var compositor:Compositor;

    private var hasGameItemSlash:Bool = false;
    private var hasGameItemExplosion:Bool = false;

    public function new(spriteSheet:BitmapData, factory:EntityFactory, compositor:Compositor) {
        super();

        this.compositor = compositor;
        this.spriteSheet = spriteSheet;
        this.factory = factory;

    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        this.engine = engine;

        nodes = engine.getNodeList(GameItemNode);

        var gameItemCanvas:Entity = engine.getEntityByName("gameItemCanvas");

        canvasRect = gameItemCanvas.get(Viewport).rect;
        canvas = gameItemCanvas.get(BitmapSource);
        bottomRight = canvasRect.bottomRight;
        topLeft = canvasRect.topLeft;

        config = engine.getEntityByName("gameSession").get(GameConfig);
        health = engine.getEntityByName("gameSession").get(Health);
    }

    override public function update(time:Float):Void {

        compositor.setCurrentTarget("sceneMap");

        if (config.running){

            for (node in nodes) {
                canvas.invalidate = true;
                hasGameItemSlash = node.entity.has(GameItemSlash);
                hasGameItemExplosion = node.entity.has(GameItemExplosion);

                if (hasGameItemSlash) {
                    // draw current frame to scratch
                    compositor.drawCurrentFrameToScratch(node.entity.get(Animation));
                } else if (hasGameItemExplosion) {
                    compositor.drawBitmapToCanvas(node.entity.get(BloomEffect).base, topLeft, node.entity.get(ColorTransformWrapper).colorTransform);
                    compositor.drawCurrentFrameToFullCanvas(node.entity.get(Animation), node.transform, topLeft);
                } else {
                    // draw layers to scratch
                    compositor.drawLayersToScratch(node.info.layers);
                }

                compositor.applyAlphaTransform(node.entity.get(AlphaTransform));

                compositor.drawTransformedToCanvas(node.transform, topLeft, true);

                if (node.entity.has(Character)) {
                    compositor.drawTextField(node.entity.get(Character).textField, node.transform, topLeft, true);
                }

                if (!hasGameItemSlash && (topLeft.y + node.transform.y) > (bottomRight.y + 200)) {
                    engine.removeEntity(node.entity);

                    if (node.info.category == GameItemCategory.NEUTRAL && node.info.layers.length == 2) {

                        factory.createMiss(node);

                        if (health.lives > 0) {
                            health.lives--;
                            health.invalidate = true;
                        }

                    }

                }

            }
        } else if(!nodes.empty) {

            for (node in nodes) {
                engine.removeEntity(node.entity);
            }

            compositor.clearTargetRegion(canvasRect);
        }

    }

}
