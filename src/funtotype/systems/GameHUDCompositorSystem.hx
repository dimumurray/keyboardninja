package funtotype.systems;

import ash.core.Engine;
import ash.core.Entity;
import ash.core.System;

import funtotype.components.GameStats;
import funtotype.components.Health;

import openfl.geom.Point;
import openfl.geom.Rectangle;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;
import openfl.text.TextFieldAutoSize;
import openfl.text.AntiAliasType;
import openfl.text.GridFitType;

import openfl.display.BitmapData;

class GameHUDCompositorSystem extends System{
    private var format:TextFormat;
    private var textField:TextField;

    private var health:Health;
    private var stats:GameStats;

    private var blankCanvasRect:Rectangle;

    private var monkeyBustRect:Rectangle;
    private var monkeyBustDest:Point;

    private var xDamageRect:Rectangle;
    private var xDamageDest:Point;

    private var pointsDest:Point;

    private var scene:BitmapData;
    private var scratch:BitmapData;

    public function new(scene:BitmapData) {
        super();
        this.scene = scene;

        scratch = new BitmapData(256, 96, true, 0x00000000);

        monkeyBustRect = new Rectangle(1832, 800, 76, 86);
        monkeyBustDest = new Point(0, 400);

        xDamageRect = new Rectangle(1850, 1003, 36, 111);
        xDamageDest = new Point(18, 492);

        pointsDest = new Point(76, 432);
        blankCanvasRect = new Rectangle(916, 800, 204, 203);

        format = new TextFormat("Wewak", 48, 0x00FFFF);
        format.align = TextFormatAlign.LEFT;

        textField = new TextField();
        textField.embedFonts = true;
        textField.autoSize = TextFieldAutoSize.LEFT;
        textField.antiAliasType = AntiAliasType.ADVANCED;
        textField.gridFitType = GridFitType.SUBPIXEL;
        textField.defaultTextFormat = format;
        textField.text = "0";
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        stats = engine.getEntityByName("gameSession").get(GameStats);
        health = engine.getEntityByName("gameSession").get(Health);

        draw();
    }

    override public function update(time:Float):Void {
        if (stats.invalidate || health.invalidate) {
            stats.invalidate = false;
            health.invalidate = false;

            draw();
        }
    }

    private function draw():Void {
        scene.copyPixels(scene, blankCanvasRect, monkeyBustDest);
        scene.copyPixels(scene, monkeyBustRect, monkeyBustDest);

        xDamageRect.offset(0, (3 - health.lives) * -37);

        scene.copyPixels(scene, xDamageRect, xDamageDest);

        xDamageRect.offset(0, (3 - health.lives) * 37);

        textField.text = "" + stats.pointTotal;

        scratch.fillRect(scratch.rect, 0x00000000);
        scratch.draw(textField);

        scene.copyPixels(scratch, scratch.rect, pointsDest);
    }

}
