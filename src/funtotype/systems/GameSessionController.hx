package funtotype.systems;

import ash.core.Engine;
import ash.core.System;
import ash.core.NodeList;

import funtotype.nodes.GameSessionNode;

import net.richardlord.input.KeyPoll;

import funtotype.enums.LessonType;
import funtotype.enums.Difficulty;
import funtotype.factories.EntityFactory;

import utils.PointColorCache;

import openfl.Assets;

import haxe.Json;

class GameSessionController extends System {
    private var keyPoll:KeyPoll;
    private var factory:EntityFactory;
    private var colorCache:PointColorCache;

    private var nodes:NodeList<GameSessionNode>;

    public function new(factory:EntityFactory, keyPoll:KeyPoll, colorCache:PointColorCache) {
        super();
        this.factory = factory;
        this.keyPoll = keyPoll;
        this.colorCache = colorCache;
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);
        nodes = engine.getNodeList(GameSessionNode);

    }

    override public function update(time:Float):Void {
        var key:String = colorCache.getTarget();

        for (node in nodes) {
            configure(key, node);
            updateFSM(node);
        }

    }

    private function configure(key:String, node:GameSessionNode):Void {

        switch(key) {

            case 'menu.selectDifficulty.buttons.easyButton':
                node.config.difficulty = Difficulty.EASY;
                node.config.fruitPerWave = 1;
                node.config.bombChanceFactor = 4;

            case 'menu.selectDifficulty.buttons.mediumButton':
                node.config.difficulty = Difficulty.MEDIUM;
                node.config.fruitPerWave = 3;
                node.config.bombChanceFactor = 3;

            case 'menu.selectDifficulty.buttons.hardButton':
                node.config.difficulty = Difficulty.HARD;
                node.config.fruitPerWave = 4;
                node.config.bombChanceFactor = 2;

            case 'menu.selectLesson.buttons.numberPadButton':
                node.config.lessonType = LessonType.NUMBERS;
                node.config.characterList = Json.parse(Assets.getText("numbers"));

            case 'menu.selectLesson.buttons.bottomRowButton':
                node.config.lessonType = LessonType.BOTTOM_ROW;
                node.config.characterList = Json.parse(Assets.getText("bottomRow"));

            case 'menu.selectLesson.buttons.topRowButton':
                node.config.lessonType = LessonType.TOP_ROW;
                node.config.characterList = Json.parse(Assets.getText("topRow"));

            case 'menu.selectLesson.buttons.homeRowButton':
                node.config.lessonType = LessonType.HOME_ROW;
                node.config.characterList = Json.parse(Assets.getText("homeRow"));

            case 'menu.selectLesson.buttons.allLettersButton':
                node.config.lessonType = LessonType.ALL_LETTERS;
                node.config.characterList = Json.parse(Assets.getText("letters"));

            case 'menu.title.buttons.soundButton', 'game.play.buttons.soundButton':
                node.audioSettings.soundEnabled = (node.audioSettings.soundEnabled)?false:true;
                node.audioSettings.soundToggled = true;
                node.audioSettings.invalidate = true;

            case 'menu.title.buttons.musicButton', 'game.play.buttons.musicButton':
                node.audioSettings.musicEnabled = (node.audioSettings.musicEnabled)?false:true;
                node.audioSettings.musicToggled = true;
                node.audioSettings.invalidate = true;

        }

    }

    private function updateFSM(node:GameSessionNode):Void {
        if (node.health.lives <= 0) {
            node.fsmInfo.nextState = "gameOver";
            node.fsmInfo.invalidate = true;
            node.health.lives = 3;
        }
    }
}