package funtotype.systems;

import ash.components.Transform2D;
import ash.components.BitmapSource;
import ash.core.Engine;
import ash.core.System;
import ash.core.NodeList;

import ash.components.Viewport;

import funtotype.nodes.DrawMenuSlashNode;
import funtotype.utils.Compositor;

import openfl.geom.Matrix;
import openfl.geom.Point;
import openfl.geom.Rectangle;

class DrawMenuSlashSystem extends System {
    private var nodes:NodeList<DrawMenuSlashNode>;
    private var sceneViewport:Rectangle;
    private var sceneTransform:Transform2D;

    private var compositor:Compositor;
    private var matrix:Matrix;
    private var canvas:BitmapSource;

    public function new(compositor:Compositor) {
        super();
        this.compositor = compositor;
        this.matrix = new Matrix();
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        nodes = engine.getNodeList(DrawMenuSlashNode);

        sceneViewport = engine.getEntityByName("scene").get(Viewport).rect;
        sceneTransform = engine.getEntityByName("scene").get(Transform2D);
        canvas = engine.getEntityByName("menu").get(BitmapSource);
    }

    override public function update(time:Float):Void {
        compositor.setCurrentTarget("menuMap");

        for (_node in nodes) {
            var node:DrawMenuSlashNode = cast(_node, DrawMenuSlashNode);

            if(sceneViewport.containsRect(node.viewport.rect) || sceneViewport.intersects(node.viewport.rect)) {
                compositor.drawFrameToCanvas(
                    node.anim.frames[node.anim.currentFrame],
                    new Point(
                        node.viewport.rect.left - sceneViewport.left,
                        node.viewport.rect.top - sceneViewport.top
                    )
                );

            }

        }

    }

}
