package funtotype.systems;

import openfl.geom.Rectangle;
import funtotype.components.GameStats;
import motion.Actuate;
import openfl.display.BitmapData;
import ash.core.System;
import ash.core.Engine;
import ash.core.Entity;
import ash.core.NodeList;

import funtotype.components.GameItemInfo;
import funtotype.components.GameConfig;

import funtotype.nodes.GameItemSpawnNode;

import funtotype.factories.EntityFactory;

class GameItemSpawnSystem extends System {
    private var nodes:NodeList<GameItemSpawnNode>;
    private var gameState:GameConfig;
    private var factory:EntityFactory;
    private var fruitData:Array<GameItemInfo>;

    private var spawnInterval:Float = 0;
    private var sum:Float = 0;
    private var stats:GameStats;

    public function new(factory:EntityFactory, fruitData:Array<GameItemInfo>, spawnInterval:Float = 3.5) {
        super();
        this.factory = factory;
        this.fruitData = fruitData;
        this.spawnInterval = spawnInterval;
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);
        // take

        gameState = engine.getEntityByName("gameSession").get(GameConfig);
        stats = engine.getEntityByName("gameSession").get(GameStats);

        nodes = engine.getNodeList(GameItemSpawnNode);

        for (node in nodes) {
            onNodeAdded(node);
        }

        nodes.nodeAdded.add(onNodeAdded);
        nodes.nodeRemoved.add(onNodeRemoved);
    }

    private function onNodeAdded(node:GameItemSpawnNode):Void {
        gameState.running = true;
    }

    private function onNodeRemoved(node:GameItemSpawnNode):Void {
        Actuate.timer(1).onComplete(stopSpawn);
    }

    private function stopSpawn():Void {
        gameState.running = false;
    }

    override public function update(time:Float):Void {
        super.update(time);

        var index:Int;
        var timeSpan:Float = 0;

        stats.timePlayed += time;

        sum += time;

        if (sum > spawnInterval) {

            for (node in nodes) {
                //trace("FruitSpawn::update() -- elapsedTime = " + time + ", accumulatedTime = " + sum);
                //trace("FruitSpawn::update() -- processing Node");
                index = 1;
                spawnItem();

                do {
                    timeSpan += 0.125 + (Math.random() * 0.5);  // Assign each spawned item a variable time slice/offset.
                    Actuate.timer(timeSpan).onComplete(spawnItem);
                    index++;
                }while(index < gameState.fruitPerWave);

            }

            sum = 0;
        }

    }

    private function spawnItem():Void {
        var bombChance:Float = Math.round(Math.random() * gameState.fruitPerWave * gameState.bombChanceFactor);
        var info:GameItemInfo = (bombChance == 1)? fruitData[12]: fruitData[Math.floor(Math.random() * 9)];
        var char:String = gameState.characterList[Math.floor(gameState.characterList.length * Math.random())];
        var entity:Entity = factory.createGameItem(info, gameState.speed, char);
        var powerUpChance:Float = Math.round(Math.random() * 80);

        if(powerUpChance >=1 && powerUpChance <= 5) {
            info =
            (powerUpChance == 1 || powerUpChance == 2)?fruitData[10]:       // Pineapple
            (powerUpChance == 3)?fruitData[9]:                              // Cherry
            (powerUpChance == 4 || powerUpChance == 5)?fruitData[11]:null;  // ice

            if (info == null) throw "Null Fruit info";

            char = gameState.characterList[Math.floor(gameState.characterList.length * Math.random())];
            factory.createGameItem(info, gameState.speed, char);
        }
    }

}
