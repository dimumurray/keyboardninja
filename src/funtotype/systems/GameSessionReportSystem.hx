package funtotype.systems;

import ash.components.BitmapSource;
import ash.core.Engine;
import ash.core.System;
import ash.core.NodeList;

import funtotype.components.GameStats;
import funtotype.nodes.GameSessionReportNode;

import openfl.geom.Point;
import openfl.geom.Rectangle;
import openfl.display.Sprite;
import openfl.display.BitmapData;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.GridFitType;
import openfl.text.AntiAliasType;
import openfl.text.TextFormatAlign;
import openfl.text.TextFieldAutoSize;

class GameSessionReportSystem extends System {
    private var nodes:NodeList<GameSessionReportNode>;

    private var stats:GameStats;
    private var scene:BitmapData;
    private var spriteSheet:BitmapData;

    private var destPoint:Point;
    private var dashboardBounds:Rectangle;

    private var container:Sprite;
    private var format:TextFormat;

    private var timePlayed:TextField;
    private var fruitsSliced:TextField;
    private var pointsScored:TextField;

    private var scratch:BitmapData;

    private var bmpSource:BitmapSource;

    public function new(spriteSheet:BitmapData, scene:BitmapData) {
        super();
        this.spriteSheet = spriteSheet;
        this.scene = scene;

        scratch = new BitmapData(130, 81, true, 0x00000000);

        container = new Sprite();

        format = new TextFormat("Wewak", 28, 0x69381f);
        format.align = TextFormatAlign.LEFT;

        timePlayed = getTextField(32, -8);
        fruitsSliced = getTextField(32, 18);
        pointsScored = getTextField(32, 44);

        dashboardBounds = new Rectangle(1814, 166, 130, 81);
        destPoint = new Point(551, 964);
    }

    private function getTextField(x:Float, y:Float):TextField {

        var textField:TextField = new TextField();
        textField.embedFonts = true;
        textField.autoSize = TextFieldAutoSize.LEFT;
        textField.antiAliasType = AntiAliasType.ADVANCED;
        textField.gridFitType = GridFitType.SUBPIXEL;
        textField.defaultTextFormat = format;
        textField.text = "";

        textField.x = x;
        textField.y = y;

        container.addChild(textField);

        return textField;
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        stats = engine.getEntityByName("gameSession").get(GameStats);
        bmpSource = engine.getEntityByName("scene").get(BitmapSource);

        nodes = engine.getNodeList(GameSessionReportNode);

        nodes.nodeAdded.add(onNodeAdded);
    }

    private function onNodeAdded(node:GameSessionReportNode):Void {
        var minutes:Int = Math.floor(stats.timePlayed/60);
        var seconds:Int = Math.floor(stats.timePlayed) % 60;

        timePlayed.text = "" + minutes + ":" + ((seconds < 10)?"0":"") + (Math.floor(stats.timePlayed) % 60);
        fruitsSliced.text = "" + stats.fruitsSliced;
        pointsScored.text = "" + stats.pointTotal;

        scratch.copyPixels(spriteSheet, dashboardBounds, scratch.rect.topLeft);
        scratch.draw(container);
        scene.copyPixels(scratch, scratch.rect, destPoint);

        bmpSource.invalidate = true;
    }
}
