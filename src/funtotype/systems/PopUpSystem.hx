package funtotype.systems;

import openfl.text.TextField;
import ash.components.AlphaTransform;
import ash.core.Engine;
import ash.core.System;
import ash.core.Entity;
import ash.core.NodeList;

import ash.components.BitmapSource;
import ash.components.Viewport;

import funtotype.nodes.PopUpNode;
import funtotype.utils.Compositor;
import funtotype.components.Character;
import funtotype.components.GameConfig;

import openfl.geom.Point;
import openfl.geom.Rectangle;

class PopUpSystem extends System {
    private var compositor:Compositor;
    private var popUpBounds:Rectangle;
    private var nodes:NodeList<PopUpNode>;
    private var config:GameConfig;

    private var canvasRect:Rectangle;
    private var canvas:BitmapSource;

    private var engine:Engine;

    private var topLeft:Point;

    public function new(compositor:Compositor) {
        super();
        this.compositor = compositor;
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);
        nodes = engine.getNodeList(PopUpNode);

        config = engine.getEntityByName("gameSession").get(GameConfig);

        var gameItemCanvas:Entity = engine.getEntityByName("gameItemCanvas");

        canvasRect = gameItemCanvas.get(Viewport).rect;
        topLeft = canvasRect.topLeft;
        canvas = gameItemCanvas.get(BitmapSource);

        this.engine = engine;
    }

    override public function update(time:Float):Void {
        var aTransform:AlphaTransform = null;
        var textField:TextField;

        compositor.setCurrentTarget("sceneMap");

        if(config.running) {

            for (node in nodes) {

                if (node.entity.has(AlphaTransform)) {
                    aTransform = node.entity.get(AlphaTransform);
                }

                compositor.drawLayersToScratch([node.popUp.layer]);

                compositor.applyAlphaTransform(aTransform);

                compositor.drawTransformedToCanvas(node.transform, topLeft);

                if (node.entity.has(Character)) {
                    textField = node.entity.get(Character).textField;
                    textField.alpha = aTransform.alpha/0xFF;
                    compositor.drawTextField(textField, node.transform, topLeft);
                }

                canvas.invalidate = true;
            }

        } else if (!nodes.empty) {

            for (node in nodes) {
                engine.removeEntity(node.entity);
            }

            compositor.clearTargetRegion(canvasRect);

        }
    }

}
