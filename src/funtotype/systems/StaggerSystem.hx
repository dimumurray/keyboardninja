package funtotype.systems;

import funtotype.utils.Compositor;
import ash.core.Engine;
import ash.core.Entity;
import ash.core.System;
import ash.core.NodeList;

import ash.components.BitmapSource;
import ash.components.Viewport;

import funtotype.nodes.StaggerNode;
import funtotype.components.GameConfig;

import openfl.geom.Point;
import openfl.geom.Rectangle;

class StaggerSystem extends System {
    private var nodes:NodeList<StaggerNode>;
    private var staggerPoint:Point;
    private var engine:Engine;

    private var canvas:BitmapSource;
    private var config:GameConfig;

    private var compositor:Compositor;
    private var canvasRect:Rectangle;

    public function new(compositor:Compositor) {
        super();
        this.compositor = compositor;
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        this.engine = engine;

        nodes = engine.getNodeList(StaggerNode);

        var gameItemCanvas:Entity = engine.getEntityByName("gameItemCanvas");
        canvas = gameItemCanvas.get(BitmapSource);
        canvasRect = gameItemCanvas.get(Viewport).rect;

        config = engine.getEntityByName("gameSession").get(GameConfig);
    }

    override public function update(time:Float):Void {
        compositor.setCurrentTarget("sceneMap");

        if (config.running) {

            for (node in nodes) {

                var len:Float = Math.random() * node.stagger.length;

                staggerPoint = Point.polar(len, Math.random() * (2 * Math.PI));

                var clone:Point = canvasRect.topLeft.clone();

                clone.offset(
                    node.transform.x - (node.viewport.rect.width * 0.5) + staggerPoint.x,
                    node.transform.y - (node.viewport.rect.height * 0.5) + staggerPoint.y
                );

                compositor.drawToCanvas(node.viewport.rect, clone);
            }

            canvas.invalidate = true;

        } else if (!nodes.empty) {

            for (node in nodes) {
                engine.removeEntity(node.entity);
            }

            compositor.clearTargetRegion(canvasRect);
        }
    }
}
