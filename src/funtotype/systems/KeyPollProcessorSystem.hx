package funtotype.systems;

import funtotype.components.Character;
import ash.components.AlphaTransform;
import ash.core.Engine;
import ash.core.System;
import ash.core.NodeList;

import funtotype.nodes.GameItemNode;
import funtotype.components.Health;
import funtotype.components.GameConfig;
import funtotype.components.GameStats;
import funtotype.enums.GameItemCategory;
import funtotype.factories.EntityFactory;

import net.richardlord.input.KeyPoll;

import motion.Actuate;
import motion.actuators.GenericActuator;

class KeyPollProcessorSystem extends System {

    private var keyPoll:KeyPoll;
    private var keysNotReleased:Array<Int>;

    private var queueMap:Map<GameItemCategory, Array<GameItemNode>>;

    private var buffs:Array<GameItemNode>;
    private var nerfs:Array<GameItemNode>;
    private var neutrals:Array<GameItemNode>;

    private var nodes:NodeList<GameItemNode>;

    private var factory:EntityFactory;
    private var engine:Engine;

    private var config:GameConfig;
    private var stats:GameStats;
    private var health:Health;
    private var iceVignetteAlpha:AlphaTransform;

    private var doublePointsActuator:GenericActuator<Dynamic>;
    private var freezeActuator:GenericActuator<Dynamic>;

    private var timeElapsed:Float;
    private var comboSliceCount:Int;

    private var comboThreshold:Float;

    private var frozen:Bool = false;

    public function new(keyPoll:KeyPoll, factory:EntityFactory) {
        super();

        this.keyPoll = keyPoll;
        this.factory = factory;

        timeElapsed = 0;
        comboThreshold = 0.75;

        queueMap = new Map<GameItemCategory, Array<GameItemNode>>();

        buffs = new Array<GameItemNode>();
        nerfs = new Array<GameItemNode>();
        neutrals = new Array<GameItemNode>();

        queueMap.set(GameItemCategory.NEUTRAL, neutrals);
        queueMap.set(GameItemCategory.BUFF, buffs);
        queueMap.set(GameItemCategory.NERF, nerfs);

        keysNotReleased = new Array<Int>();
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        this.engine = engine;

        config = engine.getEntityByName("gameSession").get(GameConfig);
        stats = engine.getEntityByName("gameSession").get(GameStats);
        health = engine.getEntityByName("gameSession").get(Health);

        iceVignetteAlpha = engine.getEntityByName("iceVignette").get(AlphaTransform);

        nodes = engine.getNodeList(GameItemNode);

        for (node in nodes) {
            onNodeAdded(node);
        }

        nodes.nodeAdded.add(onNodeAdded);
        nodes.nodeRemoved.add(onNodeRemoved);
    }

    private function onNodeAdded(node:GameItemNode):Void {
        // add node to lists
        queueMap.get(node.info.category).push(node);
    }

    private function onNodeRemoved(node:GameItemNode):Void {
        queueMap.get(node.info.category).remove(node);
    }

    override public function update(time:Float):Void {
        if (config.running) {
            timeElapsed += time;
            processKeyInput();
            trackCombo();
        } else {
            timeElapsed = 0;
            comboSliceCount = 0;
        }
    }

    private function processKeyInput():Void {

        var keysDown:Array<Int> = keyPoll.getKeysDown();
        var matches:Array<GameItemNode>;
        var match:GameItemNode = null;

        // Filter out released keys
        keysNotReleased = keysNotReleased.filter(
            function(keyCode:Int):Bool {
                return keysDown.indexOf(keyCode) != -1;
            }
        );

        // Find newly pressed keys
        keysDown = keysDown.filter(
            function(keyCode:Int):Bool {
                return keysNotReleased.indexOf(keyCode) == -1;
            }
        );

        if (keysDown.length > 0) {

            matches = buffs
                .concat(neutrals)
                .concat(nerfs)
                .filter(
                    function(node:GameItemNode):Bool {
                        return keysDown.indexOf(node.getKeyCode()) != -1;
                    }
                );

            if (matches.length == 0) {
                // invalid button pressed deduct points and make sound
                stats.gauge = -1;
                stats.pointTotal += stats.pointLoss;
                stats.invalidate = true;

                for (item in keysDown) {
                    keysNotReleased.push(item);
                }

                factory.createSoundEffect("miss" + (1 + Math.floor(Math.random() * 4)));
                return;
            }

            // process first match
            match = matches[0];

            switch(match.info.type) {
                case "cherry":
                    comboSliceCount = 0;
                    slice(match);
                    sliceAll();
                    keysNotReleased.push(match.getKeyCode());

                case "pineapple":
                    doublePoints(match);
                    slice(match);
                    keysNotReleased.push(match.getKeyCode());

                case "ice":
                    slice(match);
                    freeze();
                    keysNotReleased.push(match.getKeyCode());

                case "bomb":
                    explode(match);

                default:

                    if (timeElapsed > 5.0) {
                        comboSliceCount = 0;
                        timeElapsed = 0;
                    }

                    slice(match);
                    comboSliceCount++;
                    keysNotReleased.push(match.getKeyCode());
            }

        }

    }

    private function slice(node:GameItemNode):Void {
        factory.createSplatter(node);
        factory.slice(node);
        factory.createSoundEffect("fruitSliced");
        engine.removeEntity(node.entity);

        stats.fruitsSliced++;
        stats.gauge = 1;
        stats.pointTotal += stats.pointGain * stats.multiplier;
        stats.invalidate = true;
    }

    private function trackCombo():Void {
        if (frozen) return;

        if (timeElapsed >= comboThreshold) {

            timeElapsed = 0;

            if (comboSliceCount >= 3) {
                factory.createComboPopUp(comboSliceCount);
            }

            comboSliceCount = 0;
        }

    }

    private function sliceAll():Void {

        for (node in neutrals) {

            if (node.info.layers.length == 2) {
                slice(node);
            }

        }

    }

    private function doublePoints(node:GameItemNode):Void {
        stats.pointTotal += stats.pointGain * stats.multiplier;
        stats.multiplier = 2;

        factory.createDoublePointsPopUp();

        if (doublePointsActuator != null) {
            Actuate.stop(doublePointsActuator);
        }

        doublePointsActuator = Actuate.timer(2).onComplete(resetMultiplier);

        stats.invalidate = true;
    }

    private function resetMultiplier():Void {
        stats.multiplier = 1;
        stats.invalidate = true;
        doublePointsActuator = null;
    }

    private function freeze():Void {
        frozen = true;

        factory.createSoundEffect("freeze");

        Actuate.update(updateIceVignetteAlpha, 1 ,[0xFF], [0x00]);
        Actuate.update(updateIceVignetteAlpha, 1, [0x00], [0xFF], false)
               .onComplete(defrost)
               .delay(5);

        Actuate.tween(config, 1, {timeScale:0.5});
        Actuate.tween(config, 1,{timeScale:1}, false)
               .delay(5);

    }

    private function updateIceVignetteAlpha(v:Float):Void {

        iceVignetteAlpha.alpha = Math.round(v);
        iceVignetteAlpha.invalidate = true;

    }

    private function defrost():Void {
        timeElapsed = 0;
        comboSliceCount = 0;
        frozen = false;
    }

    private function explode(node:GameItemNode):Void {

        // health to zero...create a "white out" effect and transition to game over screen
        factory.explode(node);
        Actuate.timer(2.2).onComplete(setHealthToZero);
    }

    private function setHealthToZero():Void {
        health.lives = 0;
        health.invalidate = true;
    }

}