package funtotype.systems;

import funtotype.utils.Compositor;
import ash.core.Engine;
import ash.core.System;
import ash.components.Viewport;

import funtotype.components.GameConfig;

import openfl.geom.Rectangle;

class ClearCanvasSystem extends System {
    private var config:GameConfig;
    private var canvasRect:Rectangle;
    private var compositor:Compositor;

    public function new(compositor:Compositor) {
        super();
        this.compositor = compositor;
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        config = engine.getEntityByName("gameSession").get(GameConfig);
        canvasRect = engine.getEntityByName("gameItemCanvas").get(Viewport).rect;

    }
    override public function update(time:Float):Void {
        compositor.setCurrentTarget("sceneMap");

        if (config.running) {
            compositor.clearTargetRegion(canvasRect);
        }
    }
}
