package funtotype.systems;

import ash.core.Engine;
import ash.core.Entity;
import ash.core.System;

import funtotype.factories.EntityFactory;

import utils.PointColorCache;

class SliceButtonSystem extends System {
    private var colorCache:PointColorCache;
    private var engine:Engine;
    private var factory:EntityFactory;

    private var ids:Array<String>;

    public function new(colorCache:PointColorCache, factory:EntityFactory) {
        super();

        this.colorCache = colorCache;
        this.factory = factory;

        ids = [
            'menu.title.buttons.playGameButton', 'menu.title.buttons.howToPlayButton',
            'menu.selectDifficulty.buttons.easyButton', 'menu.selectDifficulty.buttons.mediumButton',
            'menu.selectDifficulty.buttons.hardButton', 'menu.selectLesson.buttons.numberPadButton',
            'menu.selectLesson.buttons.bottomRowButton', 'menu.selectLesson.buttons.topRowButton',
            'menu.selectLesson.buttons.homeRowButton', 'menu.selectLesson.buttons.allLettersButton',
            'game.over.buttons.mainMenuButton', 'game.over.buttons.playAgainButton'
        ];
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        this.engine = engine;
    }

    override public function update(time:Float):Void {
        super.update(time);
        var key:String = colorCache.getLastFetched();

        if (ids.indexOf(key) != -1) {
            factory.sliceMenuButton(engine.getEntityByName(key + ".mirror"));
        }

    }
}
