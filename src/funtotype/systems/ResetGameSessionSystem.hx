package funtotype.systems;

import ash.components.BitmapSource;
import ash.core.Entity;
import ash.core.Engine;
import ash.core.System;
import ash.core.NodeList;

import funtotype.nodes.ResetGameSessionNode;

import funtotype.components.Health;
import funtotype.components.GameStats;
import funtotype.components.GameConfig;

class ResetGameSessionSystem extends System {

    private var nodes:NodeList<ResetGameSessionNode>;
    private var stats:GameStats;
    private var health:Health;
    private var config:GameConfig;

    public function new() {
        super();
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        var gameSession:Entity = engine.getEntityByName("gameSession");

        stats = gameSession.get(GameStats);
        health = gameSession.get(Health);
        config = gameSession.get(GameConfig);

        nodes = engine.getNodeList(ResetGameSessionNode);

        nodes.nodeAdded.add(onNodeAdded);
    }

    private function onNodeAdded(node:ResetGameSessionNode):Void {
        stats.fruitsSliced = 0;
        stats.timePlayed = 0;
        stats.pointTotal = 0;

        config.timeScale = 1.0;

        health.lives = 3;
        health.invalidate = true;
    }
}
