package funtotype.systems;

import ash.components.AlphaTransform;
import ash.components.ColorTransformWrapper;
import ash.core.Engine;
import ash.core.System;
import ash.core.NodeList;
import ash.components.Viewport;

import funtotype.utils.Compositor;
import funtotype.nodes.SplatterNode;
import funtotype.components.GameConfig;

import openfl.geom.Point;
import openfl.geom.Rectangle;

class SplatterSystem extends System {
    private var nodes:NodeList<SplatterNode>;

    private var config:GameConfig;
    private var engine:Engine;
    private var compositor:Compositor;
    private var canvasRect:Rectangle;
    private var topLeft:Point;

    public function new(compositor:Compositor) {
        super();
        this.compositor = compositor;
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        this.engine = engine;

        nodes = engine.getNodeList(SplatterNode);
        config = engine.getEntityByName("gameSession").get(GameConfig);

        canvasRect = engine.getEntityByName("gameItemCanvas").get(Viewport).rect;
        topLeft = canvasRect.topLeft;
    }

    override public function update(time:Float):Void {
        compositor.setCurrentTarget("sceneMap");

        if (config.running) {

            for (node in nodes) {
                if (node.splatter.color != -1) {
                    // draw to scratch region
                    compositor.drawFillAlphaToScratch(node.splatter.color, node.splatter.layer.bounds);

                    compositor.applyAlphaTransform(node.entity.get(AlphaTransform));

                    // draw to canvas
                    compositor.drawTransformedToCanvas(node.transform, topLeft, true);
                }
            }

        } else if (!nodes.empty) {

            for (node in nodes) {
                engine.removeEntity(node.entity);
            }

            compositor.clearTargetRegion(canvasRect);
        }

    }


}
