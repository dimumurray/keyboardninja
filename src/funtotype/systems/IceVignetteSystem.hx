package funtotype.systems;

import openfl._v2.geom.ColorTransform;
import ash.components.BitmapSource;
import ash.core.Engine;
import ash.core.System;
import ash.core.NodeList;

import funtotype.nodes.IceVignetteNode;

import openfl.geom.Point;
import openfl.display.Bitmap;
import openfl.display.BitmapData;

import openfl.Assets;

class IceVignetteSystem extends System {
    private var nodes:NodeList<IceVignetteNode>;

    private var alphaChannel:BitmapData;
    private var iceCanvas:BitmapData;
    private var topLeft:Point;

    public function new() {
        super();

        alphaChannel = new BitmapData(916, 400, true, 0x007f7f7f);
        topLeft = new Point();
    }

    override public function addToEngine(engine:Engine):Void {
        super.addToEngine(engine);

        iceCanvas = engine.getEntityByName("iceCanvas").get(BitmapSource).bitmap;
        nodes = engine.getNodeList(IceVignetteNode);
    }

    override public function update(time:Float):Void {

        for (node in nodes) {

            if (node.aTransform.invalidate) {

                iceCanvas.copyPixels(node.source.bitmap, node.viewport.rect, node.dest.point);
                iceCanvas.merge(alphaChannel, alphaChannel.rect, topLeft, 0, 0, 0, node.aTransform.alpha);

                node.aTransform.invalidate = false;
            }

        }

    }

}

